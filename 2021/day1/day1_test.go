package main

import (
	"reflect"
	"testing"
)

// AssertEqual checks if values are equal
func assert_equal(t *testing.T, a interface{}, b interface{}) {
	if a == b {
		return
	}
	// debug.PrintStack()
	t.Errorf("Received %v (type %v), expected %v (type %v)", a, reflect.TypeOf(a), b, reflect.TypeOf(b))
}

func TestIncreases(t *testing.T) {
	testinp := []int{199, 200, 208, 210, 200, 207, 240, 269, 260, 263}
	result := increase_cnt(testinp)
	assert_equal(t, result, 7)
}

func TestSlidingIncreases(t *testing.T) {
	testinp := []int{199, 200, 208, 210, 200, 207, 240, 269, 260, 263}
	result := sliding_increase_cnt(testinp)
	assert_equal(t, result, 5)
}
