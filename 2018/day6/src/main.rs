extern crate regex;
use regex::Regex;
extern crate termion;
use termion::color;
use termion::clear;
use std::io::Error;
use std::fs::File;
// use std::io::BufRead;
use std::io::BufReader;
use std::thread;
use std::time::Duration;
use std::io::prelude::*;
use std::collections::HashMap;


fn main() -> Result<(), Error> {
    let f = File::open("input.txt")?;
    let bufread = BufReader::new(f);

    let re = Regex::new(r"(\d+), (\d+)").unwrap();
    let mut points : Vec<(usize, usize)> = Vec::new();


    for line in bufread.lines() {
        let l = line?;
        if let Some(caps) = re.captures(&l) {
            let p : (usize, usize) = (caps.get(1).unwrap().as_str().parse::<usize>().unwrap(),
            caps.get(2).unwrap().as_str().parse::<usize>().unwrap());
            points.push(p.clone());
        }
    }

    let max_x : usize =  points.clone().into_iter().map(|(x,_)| x).max().unwrap();
    let max_y : usize =  points.clone().into_iter().map(|(_,y)| y).max().unwrap();
    println!("max x {} max y {}", max_x, max_y);


    // let mut field : [[Option<usize>; max_x]; max_y] = [[None; max_x]; max_y];
    // let mut field : Vec<Vec<Option<usize>>> = Vec::new();
    let mut field : Vec<Vec<Option<usize>>> = vec![vec![None; max_y + 1]; max_x +1];
    for i in points.into_iter().enumerate(){
        println!("point: {} {:?}", i.0, i.1);
        field[(i.1).0][(i.1).1] = Some(i.0);
    }
    print_field(&field);
    // let new_field = grow_field(&field).unwrap();
    while let Some(tmp) = grow_field(&field){
        field = tmp;
        print_field(&field);
        thread::sleep(Duration::from_millis(100));
    }
    println!{""};
    print_field(&field);

    let mut remove_vec : Vec<usize> = Vec::new();

    for x in 0..max_x {
        if let Some(i) = field[x][0] {
            if !remove_vec.contains(&i) {
                remove_vec.push(i);
            }
        }
        if let Some(i) = field[x][field[0].len()-1] {
            if !remove_vec.contains(&i) {
                remove_vec.push(i);
            }
        }
    }
    for y in 0..max_y {
        if let Some(i) = field[0][y] {
            if !remove_vec.contains(&i) {
                remove_vec.push(i);
            }
        }
        if let Some(i) = field[field.len()-1][y] {
            if !remove_vec.contains(&i) {
                remove_vec.push(i);
            }
        }
    }
    println!("remove_vec: {:?}", remove_vec);
      
    let mut area_count : HashMap<usize, usize> = HashMap::new();
    for x in 0..max_x {
        for y in 0..max_y {
            if let Some(i) = field[x][y] {
                if ! remove_vec.contains(&i){
                    if let Some(count) = area_count.get_mut(&i) {
                        *count = *count + 1;
                    } else {
                        area_count.insert(i, 1);
                    }
                }
            }
        }
    }
    println!("count vec: {:?}, \nMaximum: {}", area_count, area_count.iter().map(|(k, v)| v).max().unwrap());
    Ok(())
}

fn grow_field (f: &Vec<Vec<Option<usize>>>) -> Option<Vec<Vec<Option<usize>>>> {
    let (max_x, max_y) = (f.len(), f[0].len());
    let mut new_field : Vec<Vec<Option<usize>>> = vec![vec![None; max_y]; max_x];
    let mut changes : usize = 0;
    for x in 0..max_x {
        for y in 0..max_y {
            if let Some(i) = f[x][y]{
                new_field[x][y] = Some(i);
                continue;
            }
            let mut cand : Vec<usize> = Vec::new();
            if x > 0 {
                if let Some(c) = f[x-1][y] {
                    cand.push(c);
                }
            }
            if x < max_x -1 {
                if let Some(c) = f[x+1][y] {
                    if !cand.contains(&c) {
                        cand.push(c);
                    }
                }
            }
            if y > 0 {
                if let Some(c) = f[x][y-1] {
                    if !cand.contains(&c) {
                        cand.push(c);
                    }
                }
            }
            if y < max_y -1 {
                if let Some(c) = f[x][y+1] {
                    if !cand.contains(&c) {
                        cand.push(c);
                    }
                }
            }
            if cand.len() == 1 {
                new_field[x][y] = cand.pop();
                changes = changes + 1;
            }
        }
    }
    if changes > 0 {
        Some(new_field)
    }
    else {
        None
    }
}


fn print_field(f: &Vec<Vec<Option<usize>>>) {
    print!("{}", clear::All); 
    let mut max_x = 120;
    if f.len() < max_x {
        max_x = f.len()
    }
    let mut max_y = 70;
    if f[0].len() < max_y {
        max_y = f[0].len()
    }
    for l in 0..max_y {
        for c in 0..max_x {
            if let Some(val) = f[c][l]{
                match val % 8 {
                    0 => print!("{}", color::Fg(color::Red)),
                    1 => print!("{}", color::Fg(color::Blue)),
                    2 => print!("{}", color::Fg(color::Cyan)),
                    3 => print!("{}", color::Fg(color::Green)),
                    4 => print!("{}", color::Fg(color::Magenta)),
                    5 => print!("{}", color::Fg(color::White)),
                    6 => print!("{}", color::Fg(color::Yellow)),
                    7 => print!("{}", color::Fg(color::Black)),
                    _ => print!("{}", color::Fg(color::Black)),
                }
                print!("{}{} ", val%10, color::Fg(color::Reset));
            }
            else {
                print!("{}.{} ", color::Fg(color::LightCyan), color::Fg(color::Reset));
            }
        }
        println!("");
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn simple1(){
        let mut s = "abcCde".to_string();
        let correct = "abde".to_string();
        assert_eq!(react_once(&mut s).unwrap() , correct);
    }

}
