extern crate termion;
use std::io::Error;
use std::fs::File;
use std::io::prelude::*;


fn main() -> Result<(), Error> {
    let mut f = File::open("input.txt")?;

    let mut s : String = String::new();

    f.read_to_string(&mut s).unwrap();
    println!("last cars: {}",  s.chars().skip(s.len() - 40).collect::<String>() );
    while let Some(tmp) = react_once(&mut s) {
        s = tmp;
    }
    println!("Length of fully reacted polymer: {}", s.len());
    let mut shortest_len = s.len();
    for c in (0..26).map(|x| (x + 'a' as u8) as char){
        let mut new_s : String = s.chars().filter(|x| x.to_lowercase().next().unwrap() != c ).collect();
        while let Some(tmp) = react_once(&mut new_s) {
            new_s = tmp;
        }
        if new_s.len() < shortest_len{
            shortest_len = new_s.len();
        }
        println!("without {}: len: {}", c, new_s.len());
    }
    println!("shortest length: {}", shortest_len);
    Ok(())
}

fn react_once (s: &String) -> Option<(String)>{
    let mut new_s = String::new();
    let mut changes = 0;
    let mut c_iter = s.chars().peekable();
    while let Some(c) = c_iter.next() {
        if let Some(next) = c_iter.peek() {
            if ! next.is_alphanumeric(){
                new_s.push(c);
                break; // probably eol
            }
            if (c.is_lowercase() && next.is_uppercase() && c == next.to_lowercase().next()?) ||
                (c.is_uppercase() && next.is_lowercase() && c == next.to_uppercase().next()?){
                    c_iter.next();
                    changes += 1;
                    continue;
            }
        }
            new_s.push(c);
    }
    if changes == 0 {
        None
    } else {
        Some(new_s)
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn simple1(){
        let mut s = "abcCde".to_string();
        let correct = "abde".to_string();
        assert_eq!(react_once(&mut s).unwrap() , correct);
    }

    #[test]
    fn simple2(){
        let mut s = "abcCcde".to_string();
        let correct = "abcde".to_string();
        assert_eq!(react_once(&mut s).unwrap() , correct);
    }

    #[test]
    fn simple3(){
        let mut s = "abcCcCde".to_string();
        let correct = "abde".to_string();
        assert_eq!(react_once(&mut s).unwrap() , correct);
    }

    #[test]
    fn simple4(){
        let mut s = "abcCxcCde".to_string();
        let correct = "abxde".to_string();
        assert_eq!(react_once(&mut s).unwrap() , correct);
    }

    #[test]
    fn simple5(){
        let mut s = "abcC".to_string();
        let correct = "ab".to_string();
        assert_eq!(react_once(&mut s).unwrap() , correct);
    }

    #[test]
    fn simple6(){
        let mut s = "abCc".to_string();
        let correct = "ab".to_string();
        assert_eq!(react_once(&mut s).unwrap() , correct);
    }

    #[test]
    fn simple7(){
        let mut s = "abCCc".to_string();
        let correct = "abC".to_string();
        assert_eq!(react_once(&mut s).unwrap() , correct);
    }

    #[test]
    fn simple8(){
        let mut s = "abCcB".to_string();
        let correct = "abB".to_string();
        assert_eq!(react_once(&mut s).unwrap() , correct);
    }

    #[test]
    fn no_change1(){
        let mut s = "abcc".to_string();
        assert!(react_once(&mut s).is_none());
    }

    #[test]
    fn no_change2(){
        let mut s = "aBcc".to_string();
        assert!(react_once(&mut s).is_none());
    }

    #[test]
    fn loop1(){
        let mut s = "abCcB".to_string();
        let correct = "a".to_string();
        while let Some(tmp) = react_once(&mut s) {
            s = tmp;
            println!("new str: {}", s);
        }
        assert_eq!(s , correct);
    }

    #[test]
    fn loop2(){
        let mut s = "abCcBAsdf".to_string();
        let correct = "sdf".to_string();
        while let Some(tmp) = react_once(&mut s) {
            s = tmp;
            println!("new str: {}", s);
        }
        assert_eq!(s , correct);
    }

    #[test]
    fn real1(){
        let mut s = "dabAcCaCBAcCcaDA".to_string();
        let correct = "dabCBAcaDA".to_string();
        while let Some(tmp) = react_once(&mut s) {
            s = tmp;
            println!("new str: {}", s);
        }
        assert_eq!(s , correct);
        assert_eq!(s.len(), 10);
    }

    #[test]
    fn real2(){
        let mut s = "JzZlqQLvVjO".to_string();
        let correct = "O".to_string();
        while let Some(tmp) = react_once(&mut s) {
            s = tmp;
            println!("new str: {}", s);
        }
        assert_eq!(s , correct);
        assert_eq!(s.len(), 1);
    }

}
