use std::{
    cmp,
    fs,
    fmt,
    error,
};


#[derive(Debug, PartialEq, Clone)]
enum ConstructionError {
    MalformedInput((String, usize)),
}

impl fmt::Display for ConstructionError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match &*self {
            ConstructionError::MalformedInput(i) => write!{f, "Invalid Input: {:?} ", i},
        }
    }
}

impl error::Error for ConstructionError {
    fn source(&self) -> Option<&(dyn error::Error + 'static)> {
        None
    }
}

#[derive(Debug, PartialEq, Clone)]
struct Point {
    x: isize,
    y: isize,
}

#[derive(Debug, PartialEq, Clone)]
struct Line {
    start: Point,
    end: Point,
}

#[derive(Debug, PartialEq, Clone)]
enum Orientation {
    Hor,
    Vert,
}

#[derive(Debug, PartialEq, Clone)]
enum Intersec {
    Point(Point),
    Mult(Vec<Point>),
}


fn calculate_lines (dir_vec: &Vec<(&str, usize)>) -> Result<Vec<Line>, ConstructionError> {
    let mut line_vec: Vec<Line> = Vec::new();
    let mut cur_point = Point{x: 0, y: 0};
    for dir in dir_vec {
        let mut new_point = Point{x: 0, y: 0};
        match dir.0 {
            "U" => {new_point.x = cur_point.x; new_point.y = cur_point.y + dir.1 as isize},
            "D" => {new_point.x = cur_point.x; new_point.y = cur_point.y - dir.1 as isize},
            "R" => {new_point.x = cur_point.x + dir.1 as isize; new_point.y = cur_point.y},
            "L" => {new_point.x = cur_point.x - dir.1 as isize; new_point.y = cur_point.y},
            x => return Err(ConstructionError::MalformedInput((x.to_string(), dir.1))),
        }
        line_vec.push(Line{start: cur_point.clone(), end: new_point.clone()});
        cur_point = new_point;
    }
    Ok(line_vec)
}


fn lies_between(x: isize, i: isize, j: isize) -> bool {
    x == cmp::max(x, cmp::min(i, j)) &&  x == cmp::min(x, cmp::max(i, j))
}


fn calculate_intersection(l1: &Line, l2: &Line) -> Option<Intersec> {
    let dir_l1 = if l1.start.x == l1.end.x {Orientation::Vert} else {Orientation::Hor};
    let dir_l2 = if l2.start.x == l2.end.x {Orientation::Vert} else {Orientation::Hor};

    let mut intersection = Point{x: 0, y: 0};

    match (dir_l1, dir_l2) {
        (Orientation::Hor, Orientation::Hor)   => {
            if l1.start.y != l2.start.y {
                return None
            } else {
                let mut v = Vec::new();
                // for i in cmp::max(l1.start.x, l2.start.x)..=cmp::min(l1.end.x, l2.end.x) {
                for i in  cmp::min(l1.start.x, l1.end.x)..=cmp::max(l1.start.x, l1.end.x) {
                    for j in  cmp::min(l2.start.x, l2.end.x)..=cmp::max(l2.start.x, l2.end.x) {
                        // println!("i {} j {}", i, j);
                        if i == j {
                            v.push(Point{x: i, y: l1.start.y});
                        }
                    }
                }
                if v.len() != 0 {
                    return Some(Intersec::Mult(v));
                } else {
                    return None;
                }
            }
        },
        (Orientation::Vert, Orientation::Vert) => {
            if l1.start.x != l2.start.x {
                return None
            } else {
                let mut v = Vec::new();
                for i in  cmp::min(l1.start.y, l1.end.y)..=cmp::max(l1.start.y, l1.end.y) {
                    for j in  cmp::min(l2.start.y, l2.end.y)..=cmp::max(l2.start.y, l2.end.y) {
                        if i == j {
                            v.push(Point{x: l1.start.x, y: i});
                        }
                    }
                }
                if v.len() != 0 {
                    return Some(Intersec::Mult(v));
                } else {
                    return None;
                }
            }
        },
        (Orientation::Hor, Orientation::Vert)  => {
            if lies_between(l2.start.x, l1.start.x, l1.end.x)
                && lies_between(l1.start.y, l2.start.y, l2.end.y) {
                intersection.x = l2.start.x;
                intersection.y = l1.start.y;
            }
            else {
                return None;
            }
        },
        (Orientation::Vert, Orientation::Hor)  => {
            if lies_between(l2.start.y, l1.start.y, l1.end.y)
                && lies_between(l1.start.x, l2.start.x, l2.end.x) {
                intersection.x = l1.start.x;
                intersection.y = l2.start.y;
            }
            else {
                return None;
            }
        },
        // _ => unreachable!(),
    };
    Some(Intersec::Point(intersection))
}


fn calc_intersection_points (l0: &Vec<Line>, l1: &Vec<Line>) -> Vec<Point> {
    let mut inters_vec = Vec::new();

    for l0 in l0 {
        for l1 in l1 {
            if let Some(p) = calculate_intersection(&l0, &l1) {
                match p {
                    Intersec::Point(p) => inters_vec.push(p),
                    Intersec::Mult(p) => inters_vec.append(&mut p.clone()),
                }
            }
        }
    }
    inters_vec
}


fn distance_point_line (p: &Point, l: &Line) -> Option<usize> {
    if l.start.x == l.end.x && p.x == l.start.x && lies_between(p.y, l.start.y, l.end.y) {
        Some((p.y - l.start.y).abs() as usize)
    } else if l.start.y == l.end.y && p.y == l.start.y && lies_between(p.x, l.start.x, l.end.x) {
        Some((p.x - l.start.x).abs() as usize)
    } else {
        None
    }
}


fn line_length (l: &Line) -> usize {
    if l.start.x == l.end.x {
        (l.end.y - l.start.y).abs() as usize
    } else {
        (l.end.x - l.start.x).abs() as usize
    }
}


fn length_until_point (lv: &Vec<Line>, p: &Point) -> usize {
    let mut dist  = 0;
    for l in lv {
        if let Some(d) = distance_point_line(p, l) {
            dist += d;
            break;
        } else {
            dist += line_length(l);
        }
    }
    dist
}


fn manhatt_dist (p: &Point) -> usize {
    (p.x.abs() + p.y.abs()) as usize
}

fn find_min_distance(v: &Vec<Point>) -> usize {
    let mut min_dist = std::usize::MAX;
    for p in v {
        let dist = manhatt_dist(&p);
        if dist < min_dist && dist != 0 {
            min_dist = dist;
        }
    }
    min_dist
}


fn parse_input<'a> (l: &'a str) -> Vec<(&'a str, usize)> {
    l.split(',')
        .map(|s| {
            let t = s.split_at(1);
            (t.0, t.1.parse::<usize>().unwrap())
        })
    .collect()
}



fn main() -> Result<(), Box<dyn error::Error>> {
    let f = fs::read_to_string("input.txt")?;
    let mut wires = Vec::new();
    for line in f.lines() {
        let tmp_inp_vec = parse_input(&line);
        wires.push(tmp_inp_vec);
    }

    let mut lines = Vec::new();
    for w in &wires {
        lines.push(calculate_lines(&w)?);
    }

    let points = calc_intersection_points(&lines[0], &lines[1]);
    println!("Min Distance: {}", find_min_distance(&points));

    let mut min_dist = std::usize::MAX;
    for p in &points {
        let dist = length_until_point(&lines[0], p) + length_until_point(&lines[1], p);
        if dist < min_dist && dist != 0 {
            min_dist = dist;
        }
    }

    println!("Min Distance is {}", min_dist);

    Ok(())
}


#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn line_creation_test1() {
        let inp : Vec<(&str, usize)> = vec!{("R", 10), ("U", 5), ("L", 4), ("D", 2)};
        let lines = calculate_lines(&inp).unwrap();
        let l0 = Line{start: Point{x: 0, y: 0},end: Point{x: 10, y: 0}};
        let l1 = Line{start: Point{x: 10, y: 0},end: Point{x: 10, y: 5}};
        let l2 = Line{start: Point{x: 10, y: 5},end: Point{x: 6, y: 5}};
        let l3 = Line{start: Point{x: 6, y: 5},end: Point{x: 6, y: 3}};
        assert_eq!(lines[0], l0);
        assert_eq!(lines[1], l1);
        assert_eq!(lines[2], l2);
        assert_eq!(lines[3], l3);
    }

    #[test]
    fn intersection_test1() {
        let l0 = Line{start: Point{x: 0, y: 0}, end: Point{x: 10, y: 0}};
        let l1 = Line{start: Point{x: 2, y: -2}, end: Point{x: 2, y: 5}};
        assert_eq!(calculate_intersection(&l0, &l1).unwrap(), Intersec::Point(Point{x: 2, y: 0}));
    }

    #[test]
    fn intersection_test2() {
        let l0 = Line{start: Point{x: 0, y: 0}, end: Point{x: 10, y: 0}};
        let l1 = Line{start: Point{x: 2, y: 2}, end: Point{x: 2, y: 5}};
        assert_eq!(calculate_intersection(&l0, &l1), None);
    }

    #[test]
    fn intersection_test3() {
        let l0 = Line{start: Point{x: 1, y: 0}, end: Point{x: 1, y: -10}};
        let l1 = Line{start: Point{x: -2, y: -2}, end: Point{x: 2, y: -2}};
        assert_eq!(calculate_intersection(&l0, &l1).unwrap(), Intersec::Point(Point{x: 1, y: -2}));
    }

    #[test]
    fn intersection_test4() {
        let l0 = Line{start: Point{x: 1, y: 0}, end: Point{x: 1, y: 3}};
        let l1 = Line{start: Point{x: 1, y: -1}, end: Point{x: 1, y: 2}};
        let mut p = Vec::new();
        p.push(Point{x: 1, y: 0});
        p.push(Point{x: 1, y: 1});
        p.push(Point{x: 1, y: 2});

        assert_eq!(calculate_intersection(&l0, &l1).unwrap(), Intersec::Mult(p));
    }

    #[test]
    fn intersection_test5() {
        let l0 = Line{start: Point{x: 0, y: 0}, end: Point{x: -2, y: 0}};
        let l1 = Line{start: Point{x: -1, y: 0}, end: Point{x: 2, y: 0}};
        let mut p = Vec::new();
        p.push(Point{x: -1, y: 0});
        p.push(Point{x: 0, y: 0});

        assert_eq!(calculate_intersection(&l0, &l1).unwrap(), Intersec::Mult(p));
    }

    #[test]
    fn intersection_test6() {
        let l0 = Line{start: Point{x: 0, y: 0}, end: Point{x: -2, y: 0}};
        let l1 = Line{start: Point{x: 1, y: 0}, end: Point{x: 2, y: 0}};

        assert_eq!(calculate_intersection(&l0, &l1), None);
    }

    #[test]
    fn lies_between_test1() {
        assert_eq!(lies_between(5, 1, 6), true);
        assert_eq!(lies_between(5, 1, 4), false);
        assert_eq!(lies_between(-5, 1, 6), false);
        assert_eq!(lies_between(-5, 1, -6), true);
        assert_eq!(lies_between(-5, -1, -6), true);
        assert_eq!(lies_between(5, -1, -6), false);
        assert_eq!(lies_between(5, 1, 5), true);
        assert_eq!(lies_between(1, 1, 5), true);
    }

    #[test]
    fn intersection_points_test1 () {
        let mut v0 = Vec::new();
        v0.push(Line{start: Point{x: 0, y: 0}, end: Point{x: 10, y: 0}});
        v0.push(Line{start: Point{x: 1, y: 0}, end: Point{x: 1, y: -10}});

        let mut v1 = Vec::new();
        v1.push(Line{start: Point{x: -2, y: -2}, end: Point{x: 2, y: -2}});
        v1.push(Line{start: Point{x: 2, y: -2}, end: Point{x: 2, y: 5}});

        let mut p = Vec::new();
        p.push(Point{x: 2, y: 0});
        p.push(Point{x: 1, y: -2});

        assert_eq!(calc_intersection_points(&v0, &v1), p);
    }

    #[test]
    fn point_on_line_test1 () {
        let l = Line{start: Point{x: 0, y: 0}, end: Point{x: 5, y: 0}};
        let p = Point{x: 2, y: 0};
        assert_eq!(distance_point_line(&p, &l), Some(2));
    }

    #[test]
    fn point_on_line_test2 () {
        let l = Line{start: Point{x: 0, y: 0}, end: Point{x: -5, y: 0}};
        let p = Point{x: -2, y: 0};
        assert_eq!(distance_point_line(&p, &l), Some(2));
    }

    #[test]
    fn point_on_line_test3 () {
        let l = Line{start: Point{x: 0, y: 0}, end: Point{x: -5, y: 0}};
        let p = Point{x: 2, y: 0};
        assert_eq!(distance_point_line(&p, &l), None);
    }

    #[test]
    fn point_on_line_test4 () {
        let l = Line{start: Point{x: 10, y: 0}, end: Point{x: -5, y: 0}};
        let p = Point{x: 2, y: 0};
        assert_eq!(distance_point_line(&p, &l), Some(8));
    }


    #[test]
    fn len_until_point_test1 () {
        let inp = parse_input("R8,U5,L5,D3");
        let l = calculate_lines(&inp).unwrap();
        assert_eq!(length_until_point (&l, &Point{ x: 6, y: 5}), 15);
    }

    #[test]
    fn len_until_point_test2 () {
        let inp = parse_input("U7,R6,D4,L4");
        let l = calculate_lines(&inp).unwrap();
        assert_eq!(length_until_point (&l, &Point{ x: 6, y: 5}), 15);
    }

    #[test]
    fn len_until_point_test3 () {
        let inp = parse_input("R8,U5,L5,D3");
        let l = calculate_lines(&inp).unwrap();
        assert_eq!(length_until_point (&l, &Point{ x: 3, y: 3}), 20);
    }

    #[test]
    fn len_until_point_test4 () {
        let inp = parse_input("U7,R6,D4,L4");
        let l = calculate_lines(&inp).unwrap();
        assert_eq!(length_until_point (&l, &Point{ x: 3, y: 3}), 20);
    }
}
