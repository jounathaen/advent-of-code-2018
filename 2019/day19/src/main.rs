extern crate intcode_sim;
use intcode_sim::{IntcodeSim, Signal};
extern crate crossbeam_channel;
// use crossbeam_channel::{Receiver, Sender};
use std::{collections::HashSet, error, fs, thread};

type TractorImage = HashSet<(usize, usize)>;
const X_IMG_SIZE: usize = 50;
const Y_IMG_SIZE: usize = 50;
const RECT_SIZE: usize = 100;

enum ThreadSync {
    NewRound,
    Finished,
}

fn print_tractor_image(image: &TractorImage) {
    for y in 0..Y_IMG_SIZE {
        for x in 0..Y_IMG_SIZE {
            if let Some(_) = image.get(&(x, y)) {
                print!("#");
            } else {
                print!(".");
            }
        }
        println!();
    }
}

fn part1(code: Vec<isize>) -> TractorImage {
    let mut good_picture: TractorImage = HashSet::new();
    let (mut droid, inp, outp) = IntcodeSim::new();
    let (sync_tx, sync_rx) = crossbeam_channel::bounded::<ThreadSync>(10);

    let droid_thread = {
        thread::spawn(move || loop {
            match sync_rx.recv().unwrap() {
                ThreadSync::NewRound => {}
                ThreadSync::Finished => break,
            }
            droid.load(code.clone());
            droid.execute().unwrap();
        })
    };

    let mut first_beam = 0;
    'outer: for y in 0..Y_IMG_SIZE {
        let mut found_beam = false;
        for x in first_beam..X_IMG_SIZE {
            sync_tx.send(ThreadSync::NewRound).unwrap();
            match outp.recv().unwrap() {
                Signal::WaitForInput => {
                    inp.send(x as isize).unwrap();
                    let sig = outp.recv().unwrap();
                    assert_eq!(sig, Signal::WaitForInput);
                    inp.send(y as isize).unwrap();
                    if let Signal::Output(tracted) = outp.recv().unwrap() {
                        if tracted == 1 {
                            if found_beam == false {
                                first_beam = x;
                            }
                            found_beam = true;
                            good_picture.insert((x, y));
                        } else if found_beam {
                            let sig = outp.recv().unwrap();
                            assert_eq!(sig, Signal::ProgramHalted);
                            continue 'outer;
                        }
                    } else {
                        panic!("Something wrong");
                    }
                    let sig = outp.recv().unwrap();
                    assert_eq!(sig, Signal::ProgramHalted);
                }
                Signal::Output(o) => panic!(" got {}", o as u8 as char),
                Signal::ProgramHalted => {
                    panic!("Program halted unexpected");
                    // break 'outer;
                }
            }
        }
    }
    sync_tx.send(ThreadSync::Finished).unwrap();
    let affected_fields = good_picture.len();
    println!("Nr of affected_fields by tractor beam: {}", affected_fields);
    droid_thread.join().unwrap();
    good_picture
}

fn get_slope(
    sync: &crossbeam_channel::Sender<ThreadSync>,
    outp: &crossbeam_channel::Receiver<Signal>,
    inp: &crossbeam_channel::Sender<isize>,
) -> (f64, f64) {
    let mut found_beam = false;
    let mut first_x = 0;
    let last_x;
    let mut x = 0;
    let y = 100;
    loop {
        sync.send(ThreadSync::NewRound).unwrap();
        if interact_intcode(x, y, &outp, &inp) {
            if !found_beam {
                first_x = x;
            }
            found_beam = true;
        } else {
            if found_beam {
                last_x = x - 1;
                break;
            }
        }
        x += 1;
    }
    let s1 = first_x as f64 / y as f64;
    let s2 = last_x as f64 / y as f64;
    (s1, s2)
}

fn approx_point(smaller_slope: f64, larger_slope: f64) -> (usize, usize) {
    let y_aprx = RECT_SIZE as f64 * (1.0 + smaller_slope) / (larger_slope - smaller_slope);
    let x_aprx = smaller_slope * y_aprx + RECT_SIZE as f64 * smaller_slope;
    (x_aprx as usize, y_aprx as usize)
}

fn interact_intcode(
    x: usize,
    y: usize,
    outp: &crossbeam_channel::Receiver<Signal>,
    inp: &crossbeam_channel::Sender<isize>,
) -> bool {
    let mut is_tractor = false;
    match outp.recv().unwrap() {
        Signal::WaitForInput => {
            inp.send(x as isize).unwrap();
            let sig = outp.recv().unwrap();
            assert_eq!(sig, Signal::WaitForInput);
            inp.send(y as isize).unwrap();
            if let Signal::Output(tracted) = outp.recv().unwrap() {
                if tracted == 1 {
                    is_tractor = true;
                }
            } else {
                panic!("Something wrong");
            }
            let sig = outp.recv().unwrap();
            assert_eq!(sig, Signal::ProgramHalted);
        }
        Signal::Output(o) => panic!(" got {}", o as u8 as char),
        Signal::ProgramHalted => {
            panic!("Program halted unexpected");
        }
    }
    is_tractor
}

fn part2(code: Vec<isize>) {
    let (mut droid, inp, outp) = IntcodeSim::new();
    let (sync_tx, sync_rx) = crossbeam_channel::bounded::<ThreadSync>(10);

    let droid_thread = {
        thread::spawn(move || loop {
            match sync_rx.recv().unwrap() {
                ThreadSync::NewRound => {}
                ThreadSync::Finished => break,
            }
            droid.load(code.clone());
            droid.execute().unwrap();
        })
    };

    let (s1, s2) = get_slope(&sync_tx, &outp, &inp);
    let mut p_aprx = approx_point(s1, s2);
    // Assume, we have 10% error in our approximation
    p_aprx.0 -= p_aprx.0 / 10;
    p_aprx.1 -= p_aprx.1 / 10;
    loop {
        let x = p_aprx.0;
        let y = p_aprx.1;
        // Check if the corners are inside the beam
        sync_tx.send(ThreadSync::NewRound).unwrap();
        let tracted = interact_intcode(x, y, &outp, &inp);
        sync_tx.send(ThreadSync::NewRound).unwrap();
        let tracted_ur = interact_intcode(x + 99, y, &outp, &inp);
        sync_tx.send(ThreadSync::NewRound).unwrap();
        let tracted_dl = interact_intcode(x, y + 99, &outp, &inp);
        // Stepwise adapt the point until it is the correct one
        match (tracted, tracted_dl, tracted_ur) {
            (true, true, true) => {
                println!(
                    "found spot: {:?}. Solution: {}",
                    p_aprx,
                    p_aprx.0 * 10000 + p_aprx.1
                );
                break;
            }
            (true, false, true) => {
                // println!("Point {:?} does not fit down left", p_aprx);
                p_aprx.0 += 1;
            }
            (true, true, false) => {
                // println!("Point {:?} does not fit up right", p_aprx);
                p_aprx.1 += 1;
            }
            (true, false, false) => {
                // println!("Point {:?} does neither fit down left nor up_right", p_aprx);
                p_aprx.0 += 1;
                p_aprx.1 += 1;
            }
            _ => {
                panic!("completely on the wrong track, upper left corner does not fit...");
            }
        }
    }
    sync_tx.send(ThreadSync::Finished).unwrap();
    droid_thread.join().unwrap();
}

fn main() -> Result<(), Box<dyn error::Error>> {
    let f = fs::read_to_string("input_max.txt")?;
    let mut code = Vec::new();
    for line in f.lines() {
        let mut tmp_inp_vec: Vec<isize> = line
            .split(',')
            .map(|s| s.parse::<isize>().unwrap())
            .collect();
        code.append(&mut tmp_inp_vec);
    }

    let _good_picture = part1(code.clone());
    // print_tractor_image(&_good_picture);
    part2(code);

    Ok(())
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn get_test() {}
}
