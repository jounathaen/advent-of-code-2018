extern crate custom_error;
use custom_error::custom_error;
extern crate regex;
use regex::Regex;
extern crate num_integer;
use num_integer::Integer;
#[macro_use]
extern crate lazy_static;
use std::{cmp::PartialOrd, error, fmt, fs};

custom_error! {#[derive(PartialEq,PartialOrd)] AtromechanicError
    ParsingError = "Cannot parse str to Moon Coordinates",
}

#[derive(PartialEq, Copy, Clone, Hash, Eq)]
struct Moon {
    x: i64,
    y: i64,
    z: i64,
    vx: i64,
    vy: i64,
    vz: i64,
}
impl fmt::Debug for Moon {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "Moon: (x:{} y:{} z:{}) (vx:{} vy:{} vz:{})",
            self.x, self.y, self.z, self.vx, self.vy, self.vz
        )
    }
}
impl Moon {
    fn from(s: &str) -> Result<Self, AtromechanicError> {
        lazy_static! {
            static ref RE: Regex = Regex::new(r"<x=(-??\d+), y=(-??\d+), z=(-??\d+)>").unwrap();
        }
        let matches = RE.captures(s).unwrap();
        Ok(Moon {
            x: matches.get(1).unwrap().as_str().parse().unwrap(),
            y: matches.get(2).unwrap().as_str().parse().unwrap(),
            z: matches.get(3).unwrap().as_str().parse().unwrap(),
            vx: 0,
            vy: 0,
            vz: 0,
        })
    }

    fn potential_energy(&self) -> i64 {
        i64::abs(self.x) + i64::abs(self.y) + i64::abs(self.z)
    }

    fn kinetic_energy(&self) -> i64 {
        i64::abs(self.vx) + i64::abs(self.vy) + i64::abs(self.vz)
    }

    fn total_energy(&self) -> i64 {
        self.potential_energy() * self.kinetic_energy()
    }
}

fn apply_gravity(moons: &mut Vec<Moon>) {
    for i in 0..moons.len() {
        for j in 0..moons.len() {
            if moons[i].x < moons[j].x {
                moons[i].vx += 1;
            } else if moons[i].x > moons[j].x {
                moons[i].vx -= 1;
            }

            if moons[i].y < moons[j].y {
                moons[i].vy += 1;
            } else if moons[i].y > moons[j].y {
                moons[i].vy -= 1;
            }

            if moons[i].z < moons[j].z {
                moons[i].vz += 1;
            } else if moons[i].z > moons[j].z {
                moons[i].vz -= 1;
            }
        }
    }
}

fn update_pos(moons: &mut Vec<Moon>) {
    for m in moons.iter_mut() {
        m.x += m.vx;
        m.y += m.vy;
        m.z += m.vz;
    }
}

fn apply_gravity_1d(moons: &mut Vec<(i64, i64)>) {
    for i in 0..moons.len() {
        for j in 0..moons.len() {
            if moons[i].0 < moons[j].0 {
                moons[i].1 += 1;
            } else if moons[i].0 > moons[j].0 {
                moons[i].1 -= 1;
            }
        }
    }
}

fn update_pos_1d(moons: &mut Vec<(i64, i64)>) {
    for m in moons.iter_mut() {
        m.0 += m.1;
    }
}

fn part_1(mut moons: Vec<Moon>) {
    for _ in 0..1000 {
        apply_gravity(&mut moons);
        update_pos(&mut moons);
    }
    let total_energy: i64 = moons.iter().map(|m| m.total_energy()).sum();
    println!("Total Energy after 1000 steps: {}", total_energy);
}

fn part_2(moons: Vec<Moon>) -> i64 {
    let mut steps_x: i64 = 0;
    let mut steps_y: i64 = 0;
    let mut steps_z: i64 = 0;
    let mut x_vec: Vec<(i64, i64)> = moons.iter().map(|m| (m.x, m.vx)).collect();
    let mut y_vec: Vec<(i64, i64)> = moons.iter().map(|m| (m.y, m.vy)).collect();
    let mut z_vec: Vec<(i64, i64)> = moons.iter().map(|m| (m.z, m.vz)).collect();
    let ref_x = x_vec.clone();
    let ref_y = y_vec.clone();
    let ref_z = z_vec.clone();
    while {
        apply_gravity_1d(&mut x_vec);
        update_pos_1d(&mut x_vec);
        steps_x += 1;
        x_vec != ref_x
    } {}
    while {
        apply_gravity_1d(&mut y_vec);
        update_pos_1d(&mut y_vec);
        steps_y += 1;
        y_vec != ref_y
    } {}
    while {
        apply_gravity_1d(&mut z_vec);
        update_pos_1d(&mut z_vec);
        steps_z += 1;
        z_vec != ref_z
    } {}
    println!(
        "Steps until repetition: {}",
        steps_x.lcm(&steps_y).lcm(&steps_z)
    );
    steps_x.lcm(&steps_y).lcm(&steps_z)
}

fn main() -> Result<(), Box<dyn error::Error>> {
    let f = fs::read_to_string("input.txt")?;
    let mut moons = Vec::new();
    for line in f.lines() {
        moons.push(Moon::from(line).unwrap());
    }

    part_1(moons.clone());
    part_2(moons);

    Ok(())
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn parser() {
        let m1 = Moon::from("<x=4, y=-9, z=3>").unwrap();
        let m_ref = Moon {
            x: 4,
            y: -9,
            z: 3,
            vx: 0,
            vy: 0,
            vz: 0,
        };
        assert_eq!(m1, m_ref);
    }

    #[test]
    fn apply_grav_test() {
        let mut moons = Vec::new();
        moons.push(Moon::from("<x=-1, y=0, z=2>").unwrap());
        moons.push(Moon::from("<x=2, y=-10, z=-7>").unwrap());
        moons.push(Moon::from("<x=4, y=-8, z=8>").unwrap());
        moons.push(Moon::from("<x=3, y=5, z=-1>").unwrap());
        apply_gravity(&mut moons);
        assert_eq!(moons[0].vx, 3);
        assert_eq!(moons[0].vy, -1);
        assert_eq!(moons[0].vz, -1);
        assert_eq!(moons[1].vx, 1);
        assert_eq!(moons[1].vy, 3);
        assert_eq!(moons[1].vz, 3);
        assert_eq!(moons[2].vx, -3);
        assert_eq!(moons[2].vy, 1);
        assert_eq!(moons[2].vz, -3);
    }

    #[test]
    fn update_pos_test() {
        let mut moons = Vec::new();
        moons.push(Moon::from("<x=-1, y=0, z=2>").unwrap());
        moons.push(Moon::from("<x=2, y=-10, z=-7>").unwrap());
        moons.push(Moon::from("<x=4, y=-8, z=8>").unwrap());
        moons.push(Moon::from("<x=3, y=5, z=-1>").unwrap());
        apply_gravity(&mut moons);
        update_pos(&mut moons);
        let mut moons_ref = Vec::new();
        moons_ref.push(Moon {
            x: 2,
            y: -1,
            z: 1,
            vx: 3,
            vy: -1,
            vz: -1,
        });
        moons_ref.push(Moon {
            x: 3,
            y: -7,
            z: -4,
            vx: 1,
            vy: 3,
            vz: 3,
        });
        moons_ref.push(Moon {
            x: 1,
            y: -7,
            z: 5,
            vx: -3,
            vy: 1,
            vz: -3,
        });
        moons_ref.push(Moon {
            x: 2,
            y: 2,
            z: 0,
            vx: -1,
            vy: -3,
            vz: 1,
        });

        assert_eq!(moons, moons_ref);
        for _ in 0..9 {
            apply_gravity(&mut moons);
            update_pos(&mut moons);
        }

        moons_ref.clear();
        moons_ref.push(Moon {
            x: 2,
            y: 1,
            z: -3,
            vx: -3,
            vy: -2,
            vz: 1,
        });
        moons_ref.push(Moon {
            x: 1,
            y: -8,
            z: 0,
            vx: -1,
            vy: 1,
            vz: 3,
        });
        moons_ref.push(Moon {
            x: 3,
            y: -6,
            z: 1,
            vx: 3,
            vy: 2,
            vz: -3,
        });
        moons_ref.push(Moon {
            x: 2,
            y: 0,
            z: 4,
            vx: 1,
            vy: -1,
            vz: -1,
        });
        assert_eq!(moons, moons_ref);
    }

    #[test]
    fn energy_test() {
        let mut moons = Vec::new();
        moons.push(Moon::from("<x=-1, y=0, z=2>").unwrap());
        moons.push(Moon::from("<x=2, y=-10, z=-7>").unwrap());
        moons.push(Moon::from("<x=4, y=-8, z=8>").unwrap());
        moons.push(Moon::from("<x=3, y=5, z=-1>").unwrap());
        for _ in 0..10 {
            apply_gravity(&mut moons);
            update_pos(&mut moons);
        }
        let total_energy: i64 = moons.iter().map(|m| m.total_energy()).sum();
        assert_eq!(total_energy, 179);
    }

    #[test]
    fn part2_test() {
        let mut moons = Vec::new();
        moons.push(Moon::from("<x=-8, y=-10, z=0>").unwrap());
        moons.push(Moon::from("<x=5, y=5, z=10>").unwrap());
        moons.push(Moon::from("<x=2, y=-7, z=3>").unwrap());
        moons.push(Moon::from("<x=9, y=-8, z=-3>").unwrap());
        assert_eq!(part_2(moons), 4686774924);
    }
}
