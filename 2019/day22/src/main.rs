use std::{error, fs};

fn deal_into_new_stack(deck: &mut Vec<usize>) {
    deck.reverse();
}

fn deal_with_inc(deck: &mut Vec<usize>, n: usize) {
    let mut shuffled_deck = vec![0; deck.len()];
    for i in 0..deck.len() {
        shuffled_deck[(i * n) % deck.len()] = deck[i]
    }
    deck.clear();
    deck.append(&mut shuffled_deck);
}

fn cut_n_cards(mut deck: &mut Vec<usize>, n: isize) {
    let mut cut = if n < 0 {
        deck.split_off((deck.len() as isize + n) as usize)
    } else {
        deck.split_off(n as usize)
    };
    cut.append(&mut deck);
    deck.clear();
    deck.append(&mut cut);
}

fn apply_script(mut deck: &mut Vec<usize>, inp: &String) {
    for line in inp.lines() {
        let l_split: Vec<&str> = line.split(' ').collect();
        if l_split[0] == "cut" {
            cut_n_cards(&mut deck, l_split[1].parse::<isize>().unwrap());
        } else if l_split[0] == "deal" && l_split[1] == "with" {
            assert_eq!(l_split[2], "increment");
            deal_with_inc(&mut deck, l_split[3].parse::<usize>().unwrap());
        } else if l_split[0] == "deal" && l_split[1] == "into" {
            assert_eq!(l_split[2], "new");
            assert_eq!(l_split[3], "stack");
            deal_into_new_stack(&mut deck);
        } else {
            panic!("Invalid line {}", line)
        }
    }
}

fn part1(inp: &String) {
    let decksize = 10007;
    let mut deck = Vec::new();
    deck.reserve(decksize);
    for i in 0..decksize {
        deck.push(i);
    }
    apply_script(&mut deck, inp);
    println!(
        "Pos of 2019: {:?}",
        deck.iter()
            .enumerate()
            .find(|&(_, p)| *p == 2019)
            .unwrap()
            .0
    );
}
// Shamelessly copied from rosettacode.org/wiki/Modular_inverse#Rust
fn mod_inv(a: i128, module: i128) -> i128 {
    let mut mn = (module, a);
    let mut xy = (0, 1);

    while mn.1 != 0 {
        xy = (xy.1, xy.0 - (mn.0 / mn.1) * xy.1);
        mn = (mn.1, mn.0 % mn.1);
    }

    while xy.0 < 0 {
        xy.0 += module;
    }
    xy.0
}

// calcs a^n mod modulus
fn modpow(mut a: i128, mut n: i128, modulus: i128) -> i128 {
    let mut res: i128 = 1;
    while n > 0 {
        if n & 1 == 1 {
            res = (res * a) % modulus;
        }
        n = n >> 1;
        a = (a * a) % modulus;
    }
    res
}

fn script_to_params(inp: &String, modulus: i128) -> (i128, i128) {
    let mut a: i128 = i128::from(1);
    let mut b: i128 = i128::from(0);
    for line in inp.lines().rev() {
        let l_split: Vec<&str> = line.split(' ').collect();
        if l_split[0] == "cut" {
            // c(x) =  n + x mod 10
            let n = l_split[1].parse::<i128>().unwrap();
            b = b + n % modulus;
        } else if l_split[0] == "deal" && l_split[1] == "with" {
            // i(x) =  n^-1 * x mod 10
            assert_eq!(l_split[2], "increment");
            let inv = mod_inv(l_split[3].parse::<i128>().unwrap(), modulus);
            b = b * inv % modulus;
            a = a * inv % modulus;
        } else if l_split[0] == "deal" && l_split[1] == "into" {
            // s(x)  = -1 -x mod 10
            assert_eq!(l_split[2], "new");
            assert_eq!(l_split[3], "stack");
            b = (b * -1) % modulus;
            b = b - 1 % modulus;
            a = (a * -1) % modulus;
        } else {
            panic!("Invalid line {}", line)
        }
    }
    (a, b)
}

fn apply_formula_n_times(a: i128, b: i128, n: i128, modulus: i128) -> (i128, i128) {
    // f(f(...f(x)...))) = a^n * x + b * (a^n-1 + ... + a + 1) mod modulus
    // = a^n * x + b * (a^n - 1) / (a-1)
    let a1 = modpow(a, n, modulus);
    let b1 = if a != 1 {
        let upper = modpow(a, n, modulus) - 1;
        let lower = a - 1;
        assert!((upper > 0 && lower > 0) || (upper < 0 && lower < 0));
        ((b * upper) % modulus * mod_inv(lower, modulus)) % modulus
    } else {
        b * n % modulus
    };
    (a1, b1)
}

fn part2(inp: &String, pos: i128, n: i128, modulus: i128) -> i128 {
    let (a, b) = script_to_params(&inp, modulus);
    // println!("Formula: {} * x + {}", a, b);
    let (a1, b1) = apply_formula_n_times(a, b, n, modulus);
    let sol = ((a1 * pos) + b1) % modulus;
    println!("Card at pos {}: {}", pos, sol);
    if sol < 0 {
        modulus + sol
    } else {
        sol
    }
}

fn main() -> Result<(), Box<dyn error::Error>> {
    let f = fs::read_to_string("input.txt")?;

    part1(&f);
    part2(&f, 2020, 101741582076661, 119315717514047);

    Ok(())
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn deal_stack_test() {
        let mut v = Vec::new();
        for i in 0..10 {
            v.push(i);
        }
        deal_into_new_stack(&mut v);
        assert_eq!(v[0], 9);
        assert_eq!(v[1], 8);
    }

    #[test]
    fn cut_cards_pos() {
        let mut v = Vec::new();
        for i in 0..10 {
            v.push(i);
        }
        cut_n_cards(&mut v, 3);
        assert_eq!(v[0], 3);
        assert_eq!(v[1], 4);
        assert_eq!(v[8], 1);
        assert_eq!(v[9], 2);
    }

    #[test]
    fn cut_cards_neg() {
        let mut v = Vec::new();
        for i in 0..10 {
            v.push(i);
        }
        cut_n_cards(&mut v, -4);
        assert_eq!(v[0], 6);
        assert_eq!(v[1], 7);
        assert_eq!(v[8], 4);
        assert_eq!(v[9], 5);
    }

    #[test]
    fn deal_with_inc_test() {
        let mut v = Vec::new();
        for i in 0..10 {
            v.push(i);
        }
        deal_with_inc(&mut v, 3);
        assert_eq!(v[0], 0);
        assert_eq!(v[1], 7);
        assert_eq!(v[2], 4);
        assert_eq!(v[3], 1);
    }

    #[test]
    fn apply_script_test_1() {
        let mut deck = Vec::new();
        for i in 0..10 {
            deck.push(i);
        }
        let script = concat!(
            "deal into new stack\n",
            "cut -2\n",
            "deal with increment 7\n",
            "cut 8\n",
            "cut -4\n",
            "deal with increment 7\n",
            "cut 3\n",
            "deal with increment 9\n",
            "deal with increment 3\n",
            "cut -1\n",
        )
        .to_string();

        apply_script(&mut deck, &script);
        // Result: 9 2 5 8 1 4 7 0 3 6
        assert_eq!(deck[0], 9);
        assert_eq!(deck[1], 2);
        assert_eq!(deck[2], 5);
        assert_eq!(deck[3], 8);
    }

    #[test]
    fn apply_script_test_2() {
        let mut deck = Vec::new();
        for i in 0..10 {
            deck.push(i);
        }
        let script = concat!(
            "deal with increment 7\n",
            "deal with increment 9\n",
            "cut -2\n",
        )
        .to_string();

        apply_script(&mut deck, &script);
        // Result: 6 3 0 7 4 1 8 5 2 9
        assert_eq!(deck[0], 6);
        assert_eq!(deck[1], 3);
        assert_eq!(deck[2], 0);
        assert_eq!(deck[3], 7);
    }

    #[test]
    fn part2_test_1() {
        let script = concat!(
            "deal with increment 7\n",
            "deal with increment 9\n",
            "cut -2\n",
        )
        .to_string();

        assert_eq!(part2(&script, 0, 1, 10), 6);
        assert_eq!(part2(&script, 1, 1, 10), 3);
        assert_eq!(part2(&script, 2, 1, 10), 0);
        assert_eq!(part2(&script, 3, 1, 10), 7);
        // Result: 6 3 0 7 4 1 8 5 2 9
    }

    #[test]
    fn part2_test_2() {
        let script = concat!(
            "deal into new stack\n",
            "cut -2\n",
            "deal with increment 7\n",
            "cut 8\n",
            "cut -4\n",
            "deal with increment 7\n",
            "cut 3\n",
            "deal with increment 9\n",
            "deal with increment 3\n",
            "cut -1\n",
        )
        .to_string();
        // Result: 9 2 5 8 1 4 7 0 3 6

        assert_eq!(part2(&script, 0, 1, 10), 9);
        assert_eq!(part2(&script, 1, 1, 10), 2);
        assert_eq!(part2(&script, 2, 1, 10), 5);
        assert_eq!(part2(&script, 3, 1, 10), 8);
        assert_eq!(part2(&script, 4, 1, 10), 1);
        assert_eq!(part2(&script, 5, 1, 10), 4);
        assert_eq!(part2(&script, 6, 1, 10), 7);
        assert_eq!(part2(&script, 7, 1, 10), 0);
        assert_eq!(part2(&script, 8, 1, 10), 3);
        assert_eq!(part2(&script, 9, 1, 10), 6);
    }

    #[test]
    fn part2_test_3() {
        let script = concat!(
            "cut 6\n",
            "deal with increment 7\n",
            "deal into new stack\n",
        )
        .to_string();
        // Result: 3 0 7 4 1 8 5 2 9 6

        assert_eq!(part2(&script, 0, 1, 10), 3);
        assert_eq!(part2(&script, 1, 1, 10), 0);
        assert_eq!(part2(&script, 2, 1, 10), 7);
        assert_eq!(part2(&script, 3, 1, 10), 4);
    }

    #[test]
    fn part2_test_4() {
        let script = concat!(
            "deal with increment 7\n",
            "deal into new stack\n",
            "deal into new stack\n",
        )
        .to_string();
        // Result: 0 3 6 9 2 5 8 1 4 7

        assert_eq!(part2(&script, 0, 1, 10), 0);
        assert_eq!(part2(&script, 1, 1, 10), 3);
        assert_eq!(part2(&script, 2, 1, 10), 6);
        assert_eq!(part2(&script, 3, 1, 10), 9);
    }

    #[test]
    fn part2_simple_1() {
        let script = concat!("deal into new stack\n",).to_string();

        assert_eq!(part2(&script, 0, 1, 10), 9);
        assert_eq!(part2(&script, 1, 1, 10), 8);
        assert_eq!(part2(&script, 2, 1, 10), 7);
        assert_eq!(part2(&script, 3, 1, 10), 6);

        assert_eq!(part2(&script, 0, 2, 10), 0);
        assert_eq!(part2(&script, 1, 2, 10), 1);
        assert_eq!(part2(&script, 2, 2, 10), 2);
        assert_eq!(part2(&script, 3, 2, 10), 3);

        assert_eq!(part2(&script, 0, 3, 10), 9);
        assert_eq!(part2(&script, 1, 3, 10), 8);
        assert_eq!(part2(&script, 2, 3, 10), 7);
        assert_eq!(part2(&script, 3, 3, 10), 6);

        assert_eq!(part2(&script, 0, 1000000000000000001, 10), 9);
        assert_eq!(part2(&script, 1, 1000000000000000001, 10), 8);
        assert_eq!(part2(&script, 2, 1000000000000000001, 10), 7);
        assert_eq!(part2(&script, 3, 1000000000000000001, 10), 6);
    }

    #[test]
    fn part2_simple_2() {
        let script = concat!("deal with increment 3\n",).to_string();
        // Result: 0 7 4 1 8 5 2 9 6 3

        assert_eq!(part2(&script, 0, 1, 10), 0);
        assert_eq!(part2(&script, 1, 1, 10), 7);
        assert_eq!(part2(&script, 2, 1, 10), 4);
        assert_eq!(part2(&script, 3, 1, 10), 1);
    }

    #[test]
    fn modpow_test() {
        assert_eq!(modpow(1, 2, 10), 1);
        assert_eq!(modpow(-1, 2, 10), 1);
        assert_eq!(modpow(1, 3, 10), 1);
        assert_eq!(modpow(5, 10, 10), 5);
        assert_eq!(modpow(5, 10, 8), 1);
        assert_eq!(modpow(16, 31, 71), 24);
        assert_eq!(modpow(16, 123456789123456789, 71), 25);
        assert_eq!(modpow(16, 123456789123456789123456789, 71), 60);
        assert_eq!(
            modpow(543215432154321, 123456789123456789, 887887887887),
            59134196939
        );
    }
}
