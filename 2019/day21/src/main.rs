extern crate intcode_sim;
use intcode_sim::{IntcodeSim, Signal};
use std::{
    error,
    fs,
    thread,
    // time::Duration
};

fn execute_inscript(code: Vec<isize>, mut script: std::str::Bytes) -> isize {
    let (mut droid, inp, outp) = IntcodeSim::new();
    droid.load(code);
    let droid_thread = {
        thread::spawn(move || {
            droid.execute().unwrap();
        })
    };

    let mut retval = 0;
    loop {
        match outp.recv().unwrap() {
            Signal::Output(o) => {
                if o <= 127 {
                    print!("{}", o as u8 as char);
                } else {
                    retval = o;
                }
            }
            Signal::ProgramHalted => break,
            Signal::WaitForInput => {
                let c = script.next().unwrap();
                print!("{}", c as char);
                inp.send(c as isize).unwrap();
            }
        }
    }
    droid_thread.join().unwrap();
    retval
}

fn part1(code: Vec<isize>) {
    let script = concat!(
        "NOT A J\n",
        "NOT B T\n",
        "OR T J\n",
        "NOT C T\n",
        "OR T J\n",
        "AND D J\n",
        "WALK\n"
    )
    .bytes();
    let sol = execute_inscript(code, script);
    println!("Damage to hull: {}", sol);
}

fn part2(code: Vec<isize>) {
    let script = concat!(
        // Boolean Eq to decide whether to jump:
        // ^A | ((^B | ^C) & D & (E | H))

        // (^B | ^C)
        "NOT B T\n",
        "NOT C J\n",
        "OR T J\n",
        // & D
        "AND D J\n",
        // clear T
        "NOT D T\n",
        "AND D T\n",
        // (E | H)
        "OR E T\n",
        "OR H T\n",
        // ((^B | ^C) & D & (E | H))
        "AND T J\n",
        // ^A | ...
        "NOT A T\n",
        "OR T J\n",
        "RUN\n",
    )
    .bytes();
    let sol = execute_inscript(code, script);
    println!("Damage to hull: {}", sol);
}

fn main() -> Result<(), Box<dyn error::Error>> {
    let f = fs::read_to_string("input.txt")?;
    let mut code = Vec::new();
    for line in f.lines() {
        let mut tmp_inp_vec: Vec<isize> = line
            .split(',')
            .map(|s| s.parse::<isize>().unwrap())
            .collect();
        code.append(&mut tmp_inp_vec);
    }

    part1(code.clone());
    part2(code);

    Ok(())
}

#[cfg(test)]
mod test {
    macro_rules! vec_of_strings {
        ($($x:expr),*) => (vec![$($x.to_string()),*]);
    }

    use super::*;

    #[test]
    fn get_test() {
        let img = Image {
            pixels: vec![
                ImgElem::Space,
                ImgElem::Scaffold,
                ImgElem::Space,
                ImgElem::Space,
                ImgElem::Scaffold,
                ImgElem::Scaffold,
                ImgElem::Space,
                ImgElem::Space,
                ImgElem::Robot(Dir::Down),
            ],
            size_x: 3,
            size_y: 3,
        };
        assert_eq!(img.get(1, 1), &ImgElem::Scaffold);
        assert_eq!(img.get(0, 1), &ImgElem::Space);
        assert_eq!(img.get(1, 0), &ImgElem::Scaffold);
        assert_eq!(img.get(2, 2), &ImgElem::Robot(Dir::Down));
    }

    #[test]
    fn robot_pos() {
        let img = Image {
            pixels: vec![
                ImgElem::Space,
                ImgElem::Scaffold,
                ImgElem::Space,
                ImgElem::Space,
                ImgElem::Scaffold,
                ImgElem::Scaffold,
                ImgElem::Space,
                ImgElem::Space,
                ImgElem::Robot(Dir::Down),
            ],
            size_x: 3,
            size_y: 3,
        };
        assert_eq!(img.get_robot_pos(), Some(((2, 2), Dir::Down)));
    }

    #[test]
    fn contains_scaf_in_dir_test() {
        let img = Image {
            pixels: vec![
                ImgElem::Space,
                ImgElem::Scaffold,
                ImgElem::Space,
                ImgElem::Space,
                ImgElem::Scaffold,
                ImgElem::Scaffold,
                ImgElem::Space,
                ImgElem::Space,
                ImgElem::Space,
            ],
            size_x: 3,
            size_y: 3,
        };
        println!("{:?}", img);
        assert!(img.contains_scaf_in_dir(Dir::Up, 1, 1));
        assert!(img.contains_scaf_in_dir(Dir::Right, 1, 1));
        assert!(!img.contains_scaf_in_dir(Dir::Down, 1, 1));
    }
}
