#![allow(soft_unstable)]
#![feature(test)]
extern crate test;
use std::vec::Vec;
use std::{error, fs};

type Map = Vec<Vec<bool>>;

fn slope_down(map: &Map, right: usize, down: usize) -> usize {
    let mut trees = 0;
    let mut x: usize = 0;
    let mut y: usize = 0;
    let width = map[0].len();
    while y < map.len() {
        if map[y][x % width] {
            trees += 1;
        }
        x += right;
        y += down as usize;
    }
    trees
}

fn part1(map: &Map) -> usize {
    slope_down(&map, 3, 1)
}

fn part2(map: &Map) -> usize {
    slope_down(&map, 1, 1)
        * slope_down(&map, 3, 1)
        * slope_down(&map, 5, 1)
        * slope_down(&map, 7, 1)
        * slope_down(&map, 1, 2)
}

fn main() -> Result<(), Box<dyn error::Error>> {
    let f = fs::read_to_string("input.txt")?;

    let map: Map = f
        .lines()
        .map(|l| {
            l.chars()
                .map(|c| match c {
                    '.' => false,
                    '#' => true,
                    _ => panic!("invalid input"),
                })
                .collect()
        })
        .collect();

    println!("Part 1: {}", part1(&map));
    println!("Part 2: {}", part2(&map));
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;
    use test::Bencher;

    #[bench]
    fn pt1_bench(b: &mut Bencher) {
        let f = fs::read_to_string("input.txt").unwrap();
        let map: Map = f
            .lines()
            .map(|l| {
                l.chars()
                    .map(|c| match c {
                        '.' => false,
                        '#' => true,
                        _ => panic!("invalid input"),
                    })
                    .collect()
            })
            .collect();
        b.iter(|| part1(&map));
    }

    #[bench]
    fn pt2_bench(b: &mut Bencher) {
        let f = fs::read_to_string("input.txt").unwrap();
        let map: Map = f
            .lines()
            .map(|l| {
                l.chars()
                    .map(|c| match c {
                        '.' => false,
                        '#' => true,
                        _ => panic!("invalid input"),
                    })
                    .collect()
            })
            .collect();
        b.iter(|| part2(&map));
    }

    #[bench]
    fn parse_file_bench(b: &mut Bencher) {
        let f = fs::read_to_string("input.txt").unwrap();
        b.iter(|| {
            f.lines()
                .map(|l| {
                    l.chars()
                        .map(|c| match c {
                            '.' => false,
                            '#' => true,
                            _ => panic!("invalid input"),
                        })
                        .collect()
                })
                .collect::<Map>()
        });
    }
}
