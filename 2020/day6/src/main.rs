#![allow(soft_unstable)]
#![feature(test)]
extern crate test;
use std::collections::{HashMap, HashSet};
use std::{error, fs};

fn read_questions_pt1(inp: &String) -> Vec<HashSet<char>> {
    inp.split("\n\n")
        .map(|s| s.chars().filter(|c| *c != '\n').collect::<HashSet<char>>())
        .collect()
}

fn part1(inp: &String) -> usize {
    read_questions_pt1(inp).iter().map(|hs| hs.len()).sum()
}

fn read_questions_pt2(inp: &String) -> Vec<HashSet<char>> {
    inp.split("\n\n")
        .map(|p| {
            let persons = p.lines().count();
            let mut chars = HashMap::<char, usize>::new();
            p.chars().filter(|c| *c != '\n').for_each(|c| {
                if let Some(count) = chars.get_mut(&c) {
                    *count += 1;
                } else {
                    chars
                        .insert(c, 1)
                        .and_then::<(), _>(|_| panic!("`Option` had a value when expected `None`"));
                }
            });
            chars
                .iter()
                .filter(|(_k, v)| **v == persons)
                .map(|(k, _v)| *k)
                .collect::<HashSet<char>>()
        })
        .collect()
}

fn part2(inp: &String) -> usize {
    read_questions_pt2(inp).iter().map(|hs| hs.len()).sum()
}

fn main() -> Result<(), Box<dyn error::Error>> {
    let f = fs::read_to_string("input.txt")?;

    println!("Part 1: {}", part1(&f));
    println!("Part 2: {}", part2(&f));
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;
    use test::Bencher;

    const INP: &str = r#"abc

a
b
c

ab
ac

a
a
a
a

b
"#;

    #[test]
    fn pt1_test() {
        let questions = read_questions_pt1(&INP.to_string());
        assert_eq!(questions.len(), 5);
        assert_eq!(questions[0].len(), 3);
        assert_eq!(questions[1].len(), 3);
        assert_eq!(questions[2].len(), 3);
        assert_eq!(questions[3].len(), 1);
        assert_eq!(questions[4].len(), 1);
        assert_eq!(part1(&INP.to_string()), 11);
    }

    #[test]
    fn pt2_test() {
        let questions = read_questions_pt2(&INP.to_string());
        assert_eq!(questions.len(), 5);
        assert_eq!(questions[0].len(), 3);
        assert_eq!(questions[1].len(), 0);
        assert_eq!(questions[2].len(), 1);
        assert_eq!(questions[3].len(), 1);
        assert_eq!(questions[4].len(), 1);
        assert_eq!(part2(&INP.to_string()), 6);
    }

    #[bench]
    fn pt1_bench(b: &mut Bencher) {
        let f = fs::read_to_string("input.txt").unwrap();
        b.iter(|| part1(&f));
    }

    #[bench]
    fn pt2_bench(b: &mut Bencher) {
        let f = fs::read_to_string("input.txt").unwrap();
        b.iter(|| part2(&f));
    }
}
