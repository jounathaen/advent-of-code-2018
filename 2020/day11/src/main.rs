#![allow(soft_unstable)]
#![feature(test)]
extern crate test;
use std::{error, fs};

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum Tile {
    Floor,
    Seat,
    Occupied,
}

#[derive(Debug, PartialEq, Clone)]
struct SeatingSystem {
    grid: Vec<Tile>,
    x: usize,
    y: usize,
}
impl SeatingSystem {
    fn at(&self, x: usize, y: usize) -> Tile {
        assert!(x <= self.x);
        assert!(y <= self.y);
        *self.grid.get(x + y * self.x).expect(&format!(
            "cannot access ({}, {}) which is out of bounds ({}, {})",
            x, y, self.x, self.y
        ))
    }

    fn from_str(inp: &str) -> Self {
        let mut y = 0;
        let grid: Vec<Tile> = inp
            .chars()
            .filter_map(|c| match c {
                '.' => Some(Tile::Floor),
                'L' => Some(Tile::Seat),
                '#' => Some(Tile::Occupied),
                '\n' => {
                    y += 1;
                    None
                }
                c => panic!("invalid input: {}", c),
            })
            .collect();
        let x = grid.len() / y;
        SeatingSystem { grid, x, y }
    }

    fn occupied_seats_arround(&self, x: usize, y: usize) -> usize {
        let positions = [
            x > 0 && y > 0 && self.at(x - 1, y - 1) == Tile::Occupied,
            y > 0 && self.at(x, y - 1) == Tile::Occupied,
            x < self.x - 1 && y > 0 && self.at(x + 1, y - 1) == Tile::Occupied,
            x > 0 && self.at(x - 1, y) == Tile::Occupied,
            x < self.x - 1 && self.at(x + 1, y) == Tile::Occupied,
            x > 0 && y < self.y - 1 && self.at(x - 1, y + 1) == Tile::Occupied,
            y < self.y - 1 && self.at(x, y + 1) == Tile::Occupied,
            x < self.x - 1 && y < self.y - 1 && self.at(x + 1, y + 1) == Tile::Occupied,
        ];
        positions.iter().filter(|&p| *p).count()
    }

    fn update(&mut self) -> usize {
        let mut new_grid = self.grid.clone();
        let mut changes = 0;
        self.grid.iter().enumerate().for_each(|(i, tile)| {
            let x = i % self.x;
            let y = i / self.x;
            match tile {
                Tile::Floor => {}
                Tile::Seat => {
                    if self.occupied_seats_arround(x, y) == 0 {
                        new_grid[i] = Tile::Occupied;
                        changes += 1;
                    }
                }
                Tile::Occupied => {
                    if self.occupied_seats_arround(x, y) >= 4 {
                        new_grid[i] = Tile::Seat;
                        changes += 1;
                    }
                }
            }
        });
        self.grid = new_grid;
        changes
    }

    fn occupied_seats(&self) -> usize {
        self.grid.iter().filter(|&s| *s == Tile::Occupied).count()
    }
}

fn part1(mut inp: SeatingSystem) -> usize {
    loop {
        if inp.update() == 0 {
            return inp.occupied_seats();
        }
    }
}

//fn part2(inp: &[isize], target_nr: isize) -> isize {
//42
//}

fn main() -> Result<(), Box<dyn error::Error>> {
    let f = fs::read_to_string("input.txt")?;
    let seats = SeatingSystem::from_str(&f);
    println!("Part 1: {}", part1(seats.clone()));
    //println!("Part 2: {}", part2(&nrs, pt1_sol));
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;
    use test::Bencher;

    #[test]
    fn parsing_test() {
        let seats = SeatingSystem::from_str(
            &r#"#.LL.LL.LL
LLLLLLL.LL
#.L.L..L..
LLLL.LL.LL
L.LL.LL.LL
L.LLLLL.LL
..L.L.....
L.LLLLL.LL
"#,
        );
        assert_eq!(seats.x, 10);
        assert_eq!(seats.y, 8);
        assert_eq!(seats.at(0, 0), Tile::Occupied);
        assert_eq!(seats.at(1, 0), Tile::Floor);
        assert_eq!(seats.at(2, 0), Tile::Seat);
        assert_eq!(seats.at(0, 1), Tile::Seat);
        assert_eq!(seats.at(0, 2), Tile::Occupied);
    }

    #[test]
    fn occupied_arround_test() {
        let seats = SeatingSystem::from_str(
            &r#"#.LL.LL.LL
LL#LLLL.LL
#.#.L..L..
LLL###L.LL
L.L#.#L.LL
L.L###L.LL
..L.L....#
L.LLLLL.LL
"#,
        );
        assert_eq!(seats.occupied_seats_arround(0, 0), 0);
        assert_eq!(seats.occupied_seats_arround(1, 1), 4);
        assert_eq!(seats.occupied_seats_arround(2, 1), 1);
        assert_eq!(seats.occupied_seats_arround(3, 1), 2);
        assert_eq!(seats.occupied_seats_arround(4, 4), 8);
        assert_eq!(seats.occupied_seats_arround(9, 7), 1);
        assert_eq!(seats.occupied_seats_arround(8, 7), 1);
        assert_eq!(seats.occupied_seats_arround(9, 6), 0);
        assert_eq!(seats.occupied_seats_arround(9, 0), 0);
    }

    #[test]
    fn occupied_test() {
        assert_eq!(
            SeatingSystem::from_str(
                &r#"#.LL.LL.LL
LL#LLLL.LL
#.#.L..L..
LLL###L.LL
L.L#.#L.LL
L.L###L.LL
..L.L....#
L.LLLLL.LL
"#,
            )
            .occupied_seats(),
            13
        );
    }

    #[test]
    fn update_test() {
        let mut seats = SeatingSystem::from_str(
            &r#"L.LL.LL.LL
LLLLLLL.LL
L.L.L..L..
LLLL.LL.LL
L.LL.LL.LL
L.LLLLL.LL
..L.L.....
LLLLLLLLLL
L.LLLLLL.L
L.LLLLL.LL
"#,
        );
        seats.update();

        let next_seats = SeatingSystem::from_str(
            &r#"#.##.##.##
#######.##
#.#.#..#..
####.##.##
#.##.##.##
#.#####.##
..#.#.....
##########
#.######.#
#.#####.##
"#,
        );
        assert_eq!(seats, next_seats);
    }

    #[test]
    fn pt1_test() {
        let seats = SeatingSystem::from_str(
            &r#"L.LL.LL.LL
LLLLLLL.LL
L.L.L..L..
LLLL.LL.LL
L.LL.LL.LL
L.LLLLL.LL
..L.L.....
LLLLLLLLLL
L.LLLLLL.L
L.LLLLL.LL
"#,
        );
        assert_eq!(part1(seats), 37);
    }

    //#[bench]
    //fn pt1_bench(b: &mut Bencher) {
    //let f = fs::read_to_string("input.txt").unwrap();
    //let nrs: Vec<isize> = f
    //.lines()
    //.map(|l| l.parse::<isize>().expect("malformed input file"))
    //.collect();
    //b.iter(|| part1(&nrs, 25));
    //}

    //#[bench]
    //fn pt2_bench(b: &mut Bencher) {
    //let f = fs::read_to_string("input.txt").unwrap();
    //let nrs: Vec<isize> = f
    //.lines()
    //.map(|l| l.parse::<isize>().expect("malformed input file"))
    //.collect();
    //let pt1_sol = part1(&nrs, 25);
    //b.iter(|| part2(&nrs, pt1_sol));
    //}
}
