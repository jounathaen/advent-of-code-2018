#![allow(soft_unstable)]
#![feature(test)]
extern crate test;
use std::collections::HashSet;
use std::{convert::TryFrom, error, fs};

fn decode_seat(seat_str: &str) -> (usize, usize) {
    let mut row = (0, 128);
    seat_str.chars().take(7).for_each(|c| {
        let mid = (row.1 + 1 - row.0) / 2;
        match c {
            'F' => row = (row.0, row.0 + mid),
            'B' => row = (row.0 + mid, row.1),
            _ => panic!("Invalid input"),
        }
    });
    let mut col = (0, 8);
    seat_str.chars().skip(7).take(3).for_each(|c| {
        let mid = (col.1 - col.0) / 2;
        match c {
            'L' => col = (col.0, col.0 + mid),
            'R' => col = (col.0 + mid, col.1),
            _ => panic!("Invalid input"),
        }
    });
    assert_eq!(row.0, row.1 - 1);
    assert_eq!(col.0, col.1 - 1);
    (row.1 - 1, col.1 - 1)
}

fn seat_id(row_col: (usize, usize)) -> usize {
    row_col.0 * 8 + row_col.1
}

fn part1(inp: &String) -> usize {
    inp.lines().map(|l| seat_id(decode_seat(&l))).max().unwrap()
}

fn part2(inp: &String) -> usize {
    let mut seats: HashSet<usize> = (1..126)
        .map(|r| (1..7).map(|c| seat_id((r, c))).collect::<HashSet<usize>>())
        .flatten()
        .collect();
    let mut min = usize::MAX;
    let mut max = 0;
    inp.lines().for_each(|l| {
        let seat = seat_id(decode_seat(&l));
        if seat > max {
            max = seat
        };
        if seat < min {
            min = seat
        };
        seats.remove(&seat);
    });
    *seats
        .iter()
        .filter(|s| **s < max && **s > min)
        .nth(0)
        .unwrap()
}

fn main() -> Result<(), Box<dyn error::Error>> {
    let f = fs::read_to_string("input.txt")?;

    println!("Part 1: {}", part1(&f));
    println!("Part 2: {}", part2(&f));
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;
    use test::Bencher;

    #[test]
    fn decode_test() {
        assert_eq!(decode_seat("FBFBBFFRLR"), (44, 5));
        assert_eq!(decode_seat("BFFFBBFRRR"), (70, 7));
        assert_eq!(decode_seat("FFFBBBFRRR"), (14, 7));
        assert_eq!(decode_seat("BBFFBBFRLL"), (102, 4));
    }

    #[bench]
    fn pt1_bench(b: &mut Bencher) {
        let f = fs::read_to_string("input.txt").unwrap();
        b.iter(|| part1(&f));
    }

    #[bench]
    fn pt2_bench(b: &mut Bencher) {
        let f = fs::read_to_string("input.txt").unwrap();
        b.iter(|| part2(&f));
    }
}
