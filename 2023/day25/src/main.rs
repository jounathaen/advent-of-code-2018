use petgraph::prelude::*;
use std::{
    collections::{HashMap, HashSet},
    fs,
};

type IndexGraph = GraphMap<usize, usize, Undirected>;

#[derive(Debug, Clone)]
struct LabeledGraph<'a> {
    g: IndexGraph,
    labels: Vec<Vec<&'a str>>,
}
impl<'a> LabeledGraph<'a> {
    #[cfg(test)]
    fn translate_labels(&self, labels: HashSet<&str>) -> HashSet<usize> {
        labels.iter().map(|l| self.index(l).unwrap()).collect()
    }

    #[cfg(test)]
    fn index(&self, label: &str) -> Option<usize> {
        let label_v: Vec<&str> = label.split(',').collect();
        self.labels.iter().position(|l| l == &label_v)
    }

    fn merge_nodes(&mut self, n1: usize, n2: usize) {
        let edges = Vec::from_iter(
            self.g
                .edges(n1)
                .filter(|(_start, end, _weight)| *end != n2)
                .chain(self.g.edges(n2).filter(|(_start, end, _weight)| *end != n1)),
        );

        let mut new_edges = HashMap::new();
        edges
            .iter()
            .for_each(|e| *new_edges.entry(e.1).or_insert(0) += e.2);

        self.g.remove_node(n1);
        self.g.remove_node(n2);

        let new_label = Vec::from_iter(
            self.labels[n1]
                .iter()
                .chain(self.labels[n2].iter())
                .copied(),
        );
        self.labels.push(new_label);
        let new_pos = self.labels.len() - 1;

        self.g.add_node(new_pos);
        for (dst, weight) in new_edges.iter() {
            self.g.add_edge(new_pos, *dst, *weight);
        }
    }
}

fn inp_to_graph(inp: &str) -> LabeledGraph {
    let mut graph = IndexGraph::new();
    let mut labels = Vec::<Vec<&str>>::new();
    inp.lines().for_each(|l| {
        let mut s = l.split(": ");
        let label = s.next().unwrap();
        let label_v = vec![label];
        let l_pos = labels
            .iter()
            .position(|l| l == &label_v)
            .unwrap_or_else(|| {
                labels.push(label_v);
                labels.len() - 1
            });
        s.next().unwrap().split(' ').for_each(|e| {
            let edge_label_v = vec![e];
            let edge_l_pos = labels
                .iter()
                .position(|l| l == &edge_label_v)
                .unwrap_or_else(|| {
                    labels.push(edge_label_v);
                    labels.len() - 1
                });

            graph.add_edge(l_pos, edge_l_pos, 1);
        });
    });

    LabeledGraph { g: graph, labels }
}

fn connection_count_node_to_nodes(g: &IndexGraph, n: usize, nodes: &HashSet<usize>) -> usize {
    g.neighbors(n).filter(|neigh| nodes.contains(neigh)).count()
}

fn cut_weight(g: &IndexGraph, nodes: &HashSet<usize>) -> usize {
    nodes
        .iter()
        .map(|&n| {
            g.edges(n)
                .filter_map(|(_start, end, weight)| {
                    if !nodes.contains(&end) {
                        Some(weight)
                    } else {
                        None
                    }
                })
                .sum::<usize>()
        })
        .sum::<usize>()
}

// Compute Min-Cut and return the group that produces the cut
fn stoer_wagner(graph: &LabeledGraph, min_cut_knowledge: Option<usize>) -> (usize, HashSet<usize>) {
    let mut graph = graph.clone();

    let mut min_cut: Option<(usize, HashSet<usize>)> = None;

    let mut last_added_node = graph.g.nodes().next().unwrap();

    'outer: for _ in 0..graph.g.nodes().len() - 1 {
        let mut cur_node = graph.g.nodes().next().unwrap();
        let mut s_nodes = HashSet::<usize>::new();

        for _ in 0..graph.g.nodes().count() - 1 {
            s_nodes.insert(cur_node);
            last_added_node = cur_node;

            let cur_cut = cut_weight(&graph.g, &s_nodes);
            if let Some((min_cut_size, _min_cut_nodes)) = min_cut.as_mut() {
                if cur_cut < *min_cut_size {
                    min_cut = Some((cur_cut, s_nodes.clone()));
                    // If the min_cut is already good enough, we can stop as well
                    if cur_cut == min_cut_knowledge.unwrap_or(1) {
                        break 'outer;
                    }
                }
            } else {
                min_cut = Some((cur_cut, s_nodes.clone()));
            }

            let (tightest_name, _tightness) = graph
                .g
                .nodes()
                .filter(|n| !s_nodes.contains(n))
                .map(|n| (n, connection_count_node_to_nodes(&graph.g, n, &s_nodes)))
                .max_by_key(|(_n, tightness)| *tightness)
                .unwrap();
            cur_node = tightest_name;
        }
        let remaining_node = graph.g.nodes().find(|n| !s_nodes.contains(n)).unwrap();

        graph.merge_nodes(remaining_node, last_added_node);
    }
    min_cut.unwrap()
}

fn part1(inp: &str) -> usize {
    let g = inp_to_graph(inp);

    let min_cut = stoer_wagner(&g, Some(3));
    assert_eq!(min_cut.0, 3);
    (g.g.nodes().len() - min_cut.1.len()) * min_cut.1.len()
}

//fn part2() -> str { }

fn main() {
    let f = fs::read_to_string("input.txt").unwrap();

    let pt1 = part1(&f);
    println!("part1: {pt1}");
}

#[cfg(test)]
mod tests {
    use super::*;

    const INP: &str = "jqt: rhn xhk nvd
rsh: frs pzl lsr
xhk: hfx
cmg: qnr nvd lhk bvb
rhn: xhk bvb hfx
bvb: xhk hfx
pzl: lsr hfx nvd
qnr: nvd
ntq: jqt hfx bvb xhk
nvd: lhk
lsr: lhk
rzs: qnr cmg lsr rsh
frs: qnr lhk lsr";

    #[test]
    fn parse_test() {
        let g = inp_to_graph(INP);
        assert_eq!(g.g.node_count(), 15);
        assert_eq!(g.g.edge_count(), 33);
    }

    #[test]
    fn node_to_nodes_test() {
        let g = inp_to_graph(INP);
        assert_eq!(
            connection_count_node_to_nodes(
                &g.g,
                g.index("bvb").unwrap(),
                &g.translate_labels(HashSet::from(["hfx"]))
            ),
            1
        );
        assert_eq!(
            connection_count_node_to_nodes(
                &g.g,
                g.index("bvb").unwrap(),
                &g.translate_labels(HashSet::from(["hfx", "ntq"]))
            ),
            2
        );
        assert_eq!(
            connection_count_node_to_nodes(
                &g.g,
                g.index("bvb").unwrap(),
                &g.translate_labels(HashSet::from(["hfx", "ntq", "jqt"]))
            ),
            2
        );
    }

    #[test]
    fn cut_weight_test() {
        let g = inp_to_graph(INP);
        assert_eq!(
            cut_weight(&g.g, &g.translate_labels(HashSet::from(["hfx"]))),
            5
        );
        assert_eq!(
            cut_weight(&g.g, &g.translate_labels(HashSet::from(["hfx", "ntq"]))),
            7
        );
        assert_eq!(
            cut_weight(
                &g.g,
                &g.translate_labels(HashSet::from(["bvb", "hfx", "jqt", "ntq", "rhn", "xhk"]))
            ),
            3
        );
        assert_eq!(
            cut_weight(
                &g.g,
                &g.translate_labels(HashSet::from([
                    "rzs", "lsr", "xhk", "cmg", "lhk", "jqt", "qnr", "ntq", "hfx", "bvb", "rhn",
                    "nvd", "pzl", "frs"
                ]))
            ),
            4
        );
    }

    #[test]
    fn merge_nodes_test() {
        let mut g = inp_to_graph(INP);
        let n1 = g.index("bvb").unwrap();
        let n2 = g.index("hfx").unwrap();

        g.merge_nodes(n1, n2);

        assert_eq!(g.g.node_count(), 14);
        assert_eq!(g.g.edge_count(), 29);

        assert!(!g.g.nodes().any(|n| n == n1));

        let n1_1 = g.index("bvb,hfx").unwrap();
        let n1_2 = g.index("ntq").unwrap();
        g.merge_nodes(n1_1, n1_2);
        assert_eq!(g.g.node_count(), 13);
        assert_eq!(g.g.edge_count(), 27);
    }

    #[test]
    fn stoer_wagner_test() {
        let g = inp_to_graph(INP);

        let min_cut = stoer_wagner(&g, None);
        let sol1 = g.translate_labels(HashSet::from(["bvb", "hfx", "jqt", "ntq", "rhn", "xhk"]));
        let sol2 = g.translate_labels(HashSet::from([
            "cmg", "frs", "lhk", "lsr", "nvd", "pzl", "qnr", "rsh", "rzs",
        ]));
        assert_eq!(min_cut.0, 3);
        assert!(min_cut.1 == sol1 || min_cut.1 == sol2);
    }

    #[test]
    fn part1_test() {
        assert_eq!(part1(INP), 54);
    }
}
