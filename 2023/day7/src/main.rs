use std::{cmp::Ordering, fs};

#[derive(Debug, PartialEq, PartialOrd, Ord, Eq, Copy, Clone)]
enum Card {
    Joker = 1,
    Two = 2,
    Three = 3,
    Four = 4,
    Five = 5,
    Six = 6,
    Seven = 7,
    Eight = 8,
    Nine = 9,
    Ten = 10,
    Jack = 11,
    Queen = 12,
    King = 13,
    Ace = 14,
}
impl From<char> for Card {
    fn from(c: char) -> Self {
        match c {
            '2' => Self::Two,
            '3' => Self::Three,
            '4' => Self::Four,
            '5' => Self::Five,
            '6' => Self::Six,
            '7' => Self::Seven,
            '8' => Self::Eight,
            '9' => Self::Nine,
            'T' => Self::Ten,
            'J' => Self::Jack,
            'Q' => Self::Queen,
            'K' => Self::King,
            'A' => Self::Ace,
            _ => panic!("invalid character {c:?}"),
        }
    }
}

#[derive(Debug, PartialEq, Eq)]
#[repr(u8)]
enum Type {
    HighCard(Card) = 0,
    OnePair(Card) = 1,
    TwoPair((Card, Card)) = 2,
    ThreeOfKind(Card) = 3,
    FullHouse((Card, Card)),
    FourOfKind(Card),
    FiveOfKind(Card),
}
impl Type {
    fn discriminant(&self) -> u8 {
        // SAFETY: Because `Self` is marked `repr(u8)`, its layout is a `repr(C)` `union`
        // between `repr(C)` structs, each of which has the `u8` discriminant as its first
        // field, so we can read the discriminant without offsetting the pointer.
        unsafe { *<*const _>::from(self).cast::<u8>() }
    }
}
impl Ord for Type {
    // For the order, the assigned value is not relevant
    // We could also impl PartialEq instead...
    fn cmp(&self, other: &Self) -> Ordering {
        self.discriminant().cmp(&other.discriminant())
    }
}
impl PartialOrd for Type {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

#[derive(Debug, PartialEq, Eq)]
struct Hand<const JOKER: bool> {
    cards: [Card; 5],
    bid: u64,
}
impl<const JOKER: bool> Hand<JOKER> {
    fn get_type(&self) -> Type {
        use Type::*;
        let mut cards = self.cards;
        cards.sort_by(|a, b| b.cmp(a)); // Descending sort
        let mut card_count: Vec<(Card, usize)> = cards
            .iter()
            .map(|c| (*c, cards.iter().filter(|o| o == &c).count()))
            .collect();
        card_count.dedup();
        card_count.sort_by(|a, b| b.1.cmp(&a.1));
        //dbg!(&card_count);
        match card_count[0].1 {
            5 => FiveOfKind(card_count[0].0),
            4 => FourOfKind(card_count[0].0),
            3 => {
                if card_count[1].1 == 2 {
                    FullHouse((card_count[0].0, card_count[1].0))
                } else {
                    ThreeOfKind(card_count[0].0)
                }
            }
            2 => {
                if card_count[1].1 == 2 {
                    TwoPair((card_count[0].0, card_count[1].0))
                } else {
                    OnePair(card_count[0].0)
                }
            }
            1 => HighCard(card_count[0].0),
            _ => unreachable!(),
        }
    }

    fn get_optimized_type(&self) -> Type {
        use Card::*;
        use Type::*;
        let normal_type = self.get_type();
        let has_jack = self.cards.iter().find(|&&c| c == Jack).is_some();
        if has_jack {
            let nr_jacks = self.cards.iter().filter(|&&c| c == Jack).count();
            match normal_type {
                FiveOfKind(c) => FiveOfKind(c),
                FourOfKind(c) => {
                    if c != Jack {
                        FiveOfKind(c)
                    } else {
                        let other_card = self.cards.iter().filter(|&&c| c != Jack).next().unwrap();
                        FiveOfKind(*other_card)
                    }
                }
                ThreeOfKind(c) => {
                    if c != Jack {
                        assert_eq!(nr_jacks, 1, "c: {c:?}, {self:?}");
                        FourOfKind(c) // Only one of the other cards can be a Jack
                    } else {
                        let other_card = self.cards.iter().filter(|&&c| c != Jack).next().unwrap();
                        FourOfKind(*other_card)
                    }
                }
                OnePair(c) => {
                    if c != Jack {
                        assert_eq!(nr_jacks, 1, "c: {c:?}, {self:?}");
                        ThreeOfKind(c)
                    } else {
                        let other_card = self.cards.iter().filter(|&&c| c != Jack).next().unwrap();
                        ThreeOfKind(*other_card)
                    }
                }
                HighCard(c) => {
                    assert_eq!(nr_jacks, 1, "c: {c:?}, {self:?}");
                    OnePair(c)
                }
                FullHouse((c1, c2)) => {
                    // c1 has three cards, c2 has two cards
                    if c1 == Jack {
                        FiveOfKind(c2)
                    } else {
                        FiveOfKind(c1)
                    }
                }
                TwoPair((p1, p2)) => {
                    if p1 == Jack {
                        FourOfKind(p2)
                    } else if p2 == Jack {
                        FourOfKind(p1)
                    } else {
                        FullHouse((p1, p2))
                    }
                }
            }
        } else {
            normal_type
        }
    }
}
impl<const JOKER: bool> From<&str> for Hand<JOKER> {
    fn from(s: &str) -> Self {
        let mut splitstr = s.split(' ');
        let cards = splitstr
            .next()
            .unwrap()
            .chars()
            .map(|c| c.into())
            .collect::<Vec<Card>>()
            .try_into()
            .unwrap();
        let bid = splitstr.next().unwrap().parse().unwrap();
        Self { cards, bid }
    }
}
impl<const JOKER: bool> Ord for Hand<JOKER> {
    fn cmp(&self, other: &Self) -> Ordering {
        use Ordering::*;
        let (selftype, othertype) = if JOKER {
            (self.get_optimized_type(), other.get_optimized_type())
        } else {
            (self.get_type(), other.get_type())
        };
        match selftype.cmp(&othertype) {
            Less => Less,
            Greater => Greater,
            Equal => {
                for (s, o) in self.cards.iter().zip(other.cards.iter()) {
                    let (c1, c2) = match (JOKER, s, o) {
                        (true, Card::Jack, Card::Jack) => (&Card::Joker, &Card::Joker),
                        (true, Card::Jack, o) => (&Card::Joker, o),
                        (true, s, Card::Jack) => (s, &Card::Joker),
                        (_, s, o) => (s, o),
                    };
                    match c1.cmp(c2) {
                        Less => return Less,
                        Greater => return Greater,
                        Equal => continue,
                    }
                }
                return Equal;
            }
        }
    }
}
impl<const JOKER: bool> PartialOrd for Hand<JOKER> {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

fn input_to_hands<const JOKER: bool>(inp: &str) -> Vec<Hand<JOKER>> {
    inp.lines().map(|l| l.into()).collect()
}

fn calculate_total_winnings<const JOKER: bool>(mut hands: Vec<Hand<JOKER>>) -> u64 {
    hands.sort();
    //println!("");
    //for h in hands.iter() {
    //println!(
    //"Cards: {:?}, bid: {} -> Type: {:?}",
    //h.cards,
    //h.bid,
    //h.get_optimized_type()
    //);
    //}
    hands
        .iter()
        .enumerate()
        .map(|(i, h)| (i as u64 + 1) * h.bid)
        .sum()
}

fn main() {
    let f = fs::read_to_string("input.txt").unwrap();

    let hands = input_to_hands::<false>(&f);
    let pt1 = calculate_total_winnings(hands);
    println!("part1: {pt1}");

    let hands = input_to_hands::<true>(&f);
    let pt2 = calculate_total_winnings(hands);
    println!("part2: {pt2}");
}

#[cfg(test)]
mod tests {
    use super::*;
    //use test::Bencher;

    const INP1: &str = "32T3K 765
T55J5 684
KK677 28
KTJJT 220
QQQJA 483";

    #[test]
    fn parse_games_test() {
        use Card::*;
        let sol = vec![
            Hand {
                cards: [Three, Two, Ten, Three, King],
                bid: 765,
            },
            Hand {
                cards: [Ten, Five, Five, Jack, Five],
                bid: 684,
            },
            Hand {
                cards: [King, King, Six, Seven, Seven],
                bid: 28,
            },
            Hand {
                cards: [King, Ten, Jack, Jack, Ten],
                bid: 220,
            },
            Hand {
                cards: [Queen, Queen, Queen, Jack, Ace],
                bid: 483,
            },
        ];
        let hands = input_to_hands::<false>(&INP1);
        //dbg!(&hands);
        assert_eq!(hands, sol);
    }

    #[test]
    fn get_type_test() {
        use Card::*;
        use Type::*;
        let hands = input_to_hands::<false>(&INP1);
        let sol = vec![
            OnePair(Three),
            ThreeOfKind(Five),
            TwoPair((King, Seven)),
            TwoPair((Jack, Ten)),
            ThreeOfKind(Queen),
        ];
        let types: Vec<Type> = hands.iter().map(|h| h.get_type()).collect();
        assert_eq!(types, sol);
    }

    #[test]
    fn sort_test() {
        use Card::*;
        let mut hands: [Hand<false>; 5] = [
            Hand {
                cards: [Three, Two, Ten, Three, King],
                bid: 765,
            },
            Hand {
                cards: [Ten, Five, Five, Jack, Five],
                bid: 684,
            },
            Hand {
                cards: [King, King, Six, Seven, Seven],
                bid: 28,
            },
            Hand {
                cards: [King, Ten, Jack, Jack, Ten],
                bid: 220,
            },
            Hand {
                cards: [Queen, Queen, Queen, Jack, Ace],
                bid: 483,
            },
        ];
        assert!(hands[0] < hands[1]);
        assert!(hands[0] < hands[2]);
        assert!(hands[0] < hands[3]);
        assert!(hands[0] < hands[4]);
        assert!(hands[1] < hands[4]);
        hands.sort();
        dbg!(&hands);
        assert_eq!(hands[0].cards, [Three, Two, Ten, Three, King]);
        assert_eq!(hands[1].cards, [King, Ten, Jack, Jack, Ten]);
        assert_eq!(hands[2].cards, [King, King, Six, Seven, Seven]);
        assert_eq!(hands[3].cards, [Ten, Five, Five, Jack, Five]);
        assert_eq!(hands[4].cards, [Queen, Queen, Queen, Jack, Ace]);
    }

    #[test]
    fn test_total_winnings() {
        let hands = input_to_hands::<false>(&INP1);
        assert_eq!(calculate_total_winnings(hands), 6440);
        let hands = input_to_hands::<true>(&INP1);
        assert_eq!(calculate_total_winnings(hands), 5905);
    }

    #[test]
    fn test_optimized_types() {
        use Card::*;
        use Type::*;
        let mut hands = input_to_hands::<true>(&INP1);
        let sol = vec![
            OnePair(Three),
            FourOfKind(Five),
            TwoPair((King, Seven)),
            FourOfKind(Ten),
            FourOfKind(Queen),
        ];
        let types: Vec<Type> = hands.iter().map(|h| h.get_optimized_type()).collect();
        assert_eq!(types, sol);

        hands.sort();
        let sorted_inp = "32T3K 765
KK677 28
T55J5 684
QQQJA 483
KTJJT 220";
        let hands_sol = input_to_hands::<true>(&sorted_inp);
        assert_eq!(hands, hands_sol);

        let h1 = Hand::<true>::from("QJJQ2 1");
        assert_eq!(h1.get_optimized_type(), FourOfKind(Queen));
        let h2 = Hand::<true>::from("QQQQ2 1");
        assert!(h1 < h2);

        let h1 = Hand::<true>::from("J9J92 1");
        assert_eq!(h1.get_optimized_type(), FourOfKind(Nine));
        let h2 = Hand::<true>::from("99999 1");
        assert!(h1 < h2);
    }

    #[test]
    fn reddit_input_test() {
        let inp = "2345A 1
Q2KJJ 13
Q2Q2Q 19
T3T3J 17
T3Q33 11
2345J 3
J345A 2
32T3K 5
T55J5 29
KK677 7
KTJJT 34
QQQJA 31
JJJJJ 37
JAAAA 43
AAAAJ 59
AAAAA 61
2AAAA 23
2JJJJ 53
JJJJ2 41";
        let hands = input_to_hands::<true>(inp);
        assert_eq!(calculate_total_winnings(hands), 6839);
    }
}
