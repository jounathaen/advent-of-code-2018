use std::fs;

#[derive(Debug, PartialEq)]
struct Polynomial {
    vals: Vec<i64>,
}
impl From<&str> for Polynomial {
    fn from(s: &str) -> Self {
        let vals = s.split(' ').filter_map(|s| s.parse().ok()).collect();
        Self { vals }
    }
}
impl Polynomial {
    fn is_zero(&self) -> bool {
        !self.vals.iter().any(|&i| i != 0)
    }

    fn derive(&self) -> DerivativeIterator<& i64> {
        let mut iter = self.vals.iter();
        let prev_val = *iter.next().unwrap();
        DerivativeIterator {
            parent_iter: Box::new(iter),
            prev_val,
        }
    }
    fn extrapolate_next(&self) -> i64 {
        if self.is_zero() {
            0
        } else {
            self.vals[self.vals.len() - 1]
                + self.derive().collect::<Polynomial>().extrapolate_next()
        }
    }
    fn extrapolate_prev(&self) -> i64 {
        if self.is_zero() {
            0
        } else {
            self.vals[0] - self.derive().collect::<Polynomial>().extrapolate_prev()
        }
    }
}
impl FromIterator<i64> for Polynomial {
    fn from_iter<T: IntoIterator<Item = i64>>(iter: T) -> Self {
        Self {
            vals: Vec::from_iter(iter),
        }
    }
}

struct DerivativeIterator<'a, ITM> {
    parent_iter: Box<dyn Iterator<Item = ITM> + 'a>,
    prev_val: i64,
}
impl<'a> Iterator for DerivativeIterator<'a, &'a i64> {
    type Item = i64;
    fn next(&mut self) -> Option<Self::Item> {
        let cur_val = self.parent_iter.next()?;
        let new_val = cur_val - self.prev_val;
        self.prev_val = *cur_val;
        Some(new_val)
    }
}
impl<'a> Iterator for DerivativeIterator<'a, i64> {
    type Item = i64;
    fn next(&mut self) -> Option<Self::Item> {
        let cur_val = self.parent_iter.next()?;
        let new_val = cur_val - self.prev_val;
        self.prev_val = cur_val;
        Some(new_val)
    }
}
//impl<'a> DerivativeIterator<'a, i64> {
    //fn derive(self) -> DerivativeIterator<'a, i64> {
        //let mut iter = self;
        //let prev_val = iter.next().unwrap();
        //DerivativeIterator {
            //parent_iter: Box::new(iter),
            //prev_val,
        //}
    //}
//}
#[cfg(test)]
impl<'a> DerivativeIterator<'a, &'a i64> {
    fn derive(self) -> DerivativeIterator<'a, i64> {
        let mut iter = self;
        let prev_val = iter.next().unwrap();
        DerivativeIterator {
            parent_iter: Box::new(iter),
            prev_val,
        }
    }
}

fn part1(l: &str) -> i64 {
    l.lines()
        .map(Polynomial::from)
        .map(|p| p.extrapolate_next())
        .sum()
}

fn part2(l: &str) -> i64 {
    l.lines()
        .map(Polynomial::from)
        .map(|p| p.extrapolate_prev())
        .sum()
}

fn main() {
    let f = fs::read_to_string("input.txt").unwrap();

    let pt1 = part1(&f);
    println!("part1: {}", pt1);

    let pt2 = part2(&f);
    println!("part2: {}", pt2);
}

#[cfg(test)]
mod tests {
    use super::*;
    //use test::Bencher;

    #[test]
    fn derivative_test() {
        let funcs = ["0 3 6 9 12 15", "1 3 6 10 15 21", "10 13 16 21 30 45"];
        let deriv1s = ["3  3  3  3  3", "2  3  4  5  6", "3  3  5  9 15"];
        let deriv2s = ["0   0   0   0", "1   1   1   1", "0   2   4   6"];

        for (f_str, (d1_str, d2_str)) in funcs.iter().zip(deriv1s.iter().zip(deriv2s.iter())) {
            let f = Polynomial::from(*f_str);
            //let d_test: Vec<i64> = f.derive().collect();
            //dbg!(d_test);
            //let d_test: Vec<i64> = f.derive().derive().collect();
            //dbg!(d_test);
            //panic!();
            let d1 = Polynomial::from(*d1_str);
            let d2 = Polynomial::from(*d2_str);
            assert_eq!(f.derive().collect::<Polynomial>(), d1);
            assert_eq!(f.derive().derive().collect::<Vec<i64>>(), d2.vals);
        }

        assert!(Polynomial::from(deriv2s[0]).is_zero());
        assert!(!Polynomial::from(deriv2s[1]).is_zero());

        //let f1 = Pol
    }

    #[test]
    fn extrapolate_test() {
        let funcs = ["0 3 6 9 12 15", "1 3 6 10 15 21", "10 13 16 21 30 45"];
        let sols = [18, 28, 68];
        for (p, &s) in funcs.iter().map(|&s| Polynomial::from(s)).zip(sols.iter()) {
            assert_eq!(p.extrapolate_next(), s);
        }
    }

    #[test]
    fn extrapolate_prev_test() {
        let funcs = ["0 3 6 9 12 15", "1 3 6 10 15 21", "10 13 16 21 30 45"];
        let sols = [-3, 0, 5];
        for (p, &s) in funcs.iter().map(|&s| Polynomial::from(s)).zip(sols.iter()) {
            assert_eq!(p.extrapolate_prev(), s);
        }
    }

    #[test]
    fn pt1_test() {
        let inp = "0 3 6 9 12 15
1 3 6 10 15 21
10 13 16 21 30 45";
        assert_eq!(part1(inp), 114);
    }
}
