use std::{collections::HashMap, fmt::Debug, fs};

#[derive(PartialEq, Copy, Clone, Eq, Hash)]
enum SpringState {
    Operational,
    Damaged,
    Unknown,
}
impl From<&u8> for SpringState {
    fn from(value: &u8) -> Self {
        match value {
            b'.' => Self::Operational,
            b'#' => Self::Damaged,
            b'?' => Self::Unknown,
            _ => panic!("Invalid input: {}", *value as char),
        }
    }
}
impl Debug for SpringState {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let c = match self {
            SpringState::Operational => '.',
            SpringState::Damaged => '#',
            SpringState::Unknown => '?',
        };
        write!(f, "{c}")
    }
}

#[derive(Debug, PartialEq, Clone)]
struct SpringRow {
    spring_list: Vec<SpringState>,
    spring_groups: Vec<usize>,
}
impl SpringRow {
    fn get_nr_solutions(&self) -> usize {
        generate_potential_spring_lists(
            self.spring_list.len(),
            &self.spring_groups,
            &self.spring_list,
            &mut HashMap::new(),
        )
    }

    fn simplify(&mut self) {
        self.spring_list
            .dedup_by(|a, b| a == &SpringState::Operational && b == &SpringState::Operational);
    }
}
impl From<&str> for SpringRow {
    fn from(l: &str) -> Self {
        let mut parts = l.split(' ');
        let spring_list = str_to_springlist(parts.next().unwrap());
        let spring_groups = parts
            .next()
            .unwrap()
            .split(',')
            .map(|c| c.parse().unwrap())
            .collect();
        Self {
            spring_list,
            spring_groups,
        }
    }
}

fn str_to_springlist(s: &str) -> Vec<SpringState> {
    s.as_bytes().iter().map(SpringState::from).collect()
}

fn generate_potential_spring_lists(
    len: usize,
    grps: &[usize],
    knowledge: &[SpringState],
    cache: &mut HashMap<(Vec<usize>, Vec<SpringState>), usize>,
) -> usize {
    assert_eq!(len, knowledge.len());

    let mut sols = 0;
    if grps.is_empty() {
        // No more groups => All remaining unknowns are Functional
        return 1;
    }
    let total_group_len = grps.len() - 1 + grps.iter().sum::<usize>();
    let degrees_of_freedom = len - total_group_len;

    'outer: for i in 0..=degrees_of_freedom {
        //inserting {i}/{degrees_of_freedom} spaces in front

        let mut s: Vec<SpringState> = Vec::with_capacity(i + grps[0] + 1);

        s.extend((0..i).map(|_| SpringState::Operational));
        s.extend((0..grps[0]).map(|_| SpringState::Damaged));
        if s.len() < len {
            s.push(SpringState::Operational);
        }

        for (s, k) in s.iter().zip(knowledge.iter()) {
            if *k != SpringState::Unknown && s != k {
                // {indent} doesn't fit knowledge
                continue 'outer;
            }
        }
        let dmg_remainder = knowledge[s.len()..]
            .iter()
            .filter(|&k| k == &SpringState::Damaged)
            .count();
        let grp_remainder = grps[1..].iter().sum();
        if dmg_remainder > grp_remainder {
            // indent doesn't satisfy remaining knowledge
            continue 'outer;
        }

        let g = Vec::from(&grps[1..]);
        let k = Vec::from(&knowledge[s.len()..]);
        let pot_subsols = if let Some(sol) = cache.get(&(g.clone(), k.clone())) {
            *sol
        } else {
            let sol = generate_potential_spring_lists(len - s.len(), &g, &k, cache);
            cache.insert((g, k), sol);
            sol
        };
        sols += pot_subsols;
    }

    sols
}

fn part1(srs: &[SpringRow]) -> usize {
    srs.iter().map(|sr| sr.get_nr_solutions()).sum()
}

fn part2(srs: &[SpringRow]) -> usize {
    srs.iter()
        .map(|sr| {
            let unf_sprin_list = {
                let mut v = Vec::with_capacity(sr.spring_list.len() + 4);
                for _ in 0..4 {
                    v.extend_from_slice(&sr.spring_list);
                    v.push(SpringState::Unknown);
                }
                v.extend_from_slice(&sr.spring_list);
                v
            };
            let mut unfolded = SpringRow {
                spring_list: unf_sprin_list,
                spring_groups: Vec::from_iter(
                    std::iter::repeat(sr.spring_groups.iter())
                        .take(5)
                        .flatten()
                        .copied(),
                ),
            };
            unfolded.simplify();
            unfolded.get_nr_solutions()
        })
        .sum()
}

fn main() {
    let f = fs::read_to_string("input.txt").unwrap();

    let srs: Vec<SpringRow> = f.lines().map(SpringRow::from).collect();
    let pt1 = part1(&srs);
    println!("part1: {pt1}");

    let pt2 = part2(&srs);
    println!("part2: {pt2}");
}

#[cfg(test)]
mod tests {
    use super::*;

    const INP: &str = "???.### 1,1,3
.??..??...?##. 1,1,3
?#?#?#?#?#?#?#? 1,3,1,6
????.#...#... 4,1,1
????.######..#####. 1,6,5
?###???????? 3,2,1";

    #[test]
    fn parse_test() {
        use SpringState::*;
        let s = SpringRow::from("#.#.#?# 1,1,3");
        let sol = SpringRow {
            spring_list: vec![
                Damaged,
                Operational,
                Damaged,
                Operational,
                Damaged,
                Unknown,
                Damaged,
            ],
            spring_groups: vec![1, 1, 3],
        };
        assert_eq!(s, sol);
    }

    #[test]
    fn generate_potential_spring_lists_test() {
        let k1 = str_to_springlist("???????");
        let sl1 = generate_potential_spring_lists(7, &[1, 1, 3], &k1, &mut HashMap::new());
        assert_eq!(sl1, 1);

        let k2 = str_to_springlist("????????");
        let sl2 = generate_potential_spring_lists(8, &[1, 1, 3], &k2, &mut HashMap::new());
        assert_eq!(sl2, 4);

        println!("===========================");
        let k3 = str_to_springlist(".???????");
        let sl3 = generate_potential_spring_lists(8, &[1, 1, 3], &k3, &mut HashMap::new());
        assert_eq!(sl3, 1);

        println!("===========================");
        let k4 = str_to_springlist("#???????");
        let sl4 = generate_potential_spring_lists(8, &[1, 1, 3], &k4, &mut HashMap::new());
        assert_eq!(sl4, 3);

        for (inp, sol) in INP.lines().zip([1, 4, 1, 1, 4, 10].iter()) {
            println!("===========================");
            let mut sr = SpringRow::from(inp);
            //println!("sr       : {sr:?}");
            sr.simplify();
            //println!("sr_ simpl: {sr:?}");
            assert_eq!(sr.get_nr_solutions(), *sol);
        }
    }

    #[test]
    fn part1_test() {
        let srs: Vec<SpringRow> = INP.lines().map(SpringRow::from).collect();
        assert_eq!(part1(&srs), 21)
    }

    #[test]
    fn generate_potential_spring_lists_test_2() {
        println!("\n===========================");
        let sr = SpringRow::from("###.### 3,3");
        assert_eq!(sr.get_nr_solutions(), 1);

        println!("\n===========================");
        let sr = SpringRow::from("###.#.# 3,3");
        assert_eq!(sr.get_nr_solutions(), 0);

        println!("\n===========================");
        println!("???#??.??????.??#.. 4,3");
        let sr = SpringRow::from("???#??.??????.??#.. 4,3");
        assert_eq!(sr.get_nr_solutions(), 3);
    }

    //#[test]
    //fn part2_test() {
    //let srs: Vec<SpringRow> = INP.lines().map(SpringRow::from).collect();
    //assert_eq!(part2(&srs), 525152);
    //}
}
