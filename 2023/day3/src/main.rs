use std::{fmt, fmt::Debug, fs};

fn minim(a: u32, b: u32) -> u32 {
    if a < b {
        a
    } else {
        b
    }
}

#[derive(Debug, PartialEq)]
struct PartNr {
    nr: u32,
    line: usize,
    col: usize,
}

#[derive(Debug, PartialEq)]
struct Gear {
    line: usize,
    col: usize,
}

struct Schematic {
    field: Vec<Vec<char>>,
}
impl Schematic {
    fn part_nums_in_line(&self, line: usize) -> Vec<PartNr> {
        let mut pot_nrs: Vec<PartNr> = Vec::new();
        let mut cur_nr: String = String::new();
        let mut cur_start = None;
        for (j, c) in self.field[line].iter().enumerate() {
            match c {
                '0'..='9' => {
                    cur_nr.push(*c);
                    if cur_start.is_none() {
                        cur_start = Some(j);
                    }
                }
                _ => {
                    if cur_nr.len() > 0 {
                        let pot_nr = PartNr {
                            nr: cur_nr.parse().unwrap(),
                            line,
                            col: cur_start.unwrap(),
                        };
                        pot_nrs.push(pot_nr);
                        cur_nr = String::new();
                        cur_start = None;
                    }
                }
            }
        }
        if cur_nr.len() > 0 {
            let pot_nr = PartNr {
                nr: cur_nr.parse().unwrap(),
                line,
                col: cur_start.unwrap(),
            };
            pot_nrs.push(pot_nr);
        }
        pot_nrs
    }

    fn pot_part_numbers(&self) -> Vec<PartNr> {
        let mut pot_nrs: Vec<PartNr> = Vec::new();
        for i in 0..self.field.len() {
            pot_nrs.append(&mut self.part_nums_in_line(i));
        }
        pot_nrs
    }

    fn is_valid_part_nr(&self, pt_nr: &PartNr) -> bool {
        let start_col = if pt_nr.col == 0 {
            0
        } else {
            if self.field[pt_nr.line][pt_nr.col - 1] != '.' {
                return true;
            }
            pt_nr.col - 1
        };
        let end_col = pt_nr.col + pt_nr.nr.to_string().len();
        let end_col = if end_col >= self.field[0].len() - 1 {
            self.field[0].len() - 1
        } else {
            if self.field[pt_nr.line][end_col] != '.' {
                return true;
            }
            end_col
        };
        if pt_nr.line > 0 {
            for c in &self.field[pt_nr.line - 1][start_col..=end_col] {
                if *c != '.' {
                    return true;
                }
            }
        }
        if pt_nr.line < self.field.len() - 1 {
            for c in &self.field[pt_nr.line + 1][start_col..=end_col] {
                if *c != '.' {
                    return true;
                }
            }
        }
        false
    }

    fn find_gears(&self) -> Vec<Gear> {
        let mut gears = Vec::new();
        for (i, l) in self.field.iter().enumerate() {
            for (j, c) in l.iter().enumerate() {
                if *c == '*' {
                    gears.push(Gear { line: i, col: j });
                }
            }
        }
        gears
    }

    fn gear_ratio(&self, g: &Gear) -> Option<u32> {
        assert_eq!(self.field[g.line][g.col], '*');
        let mut nums: Vec<u32> = Vec::new();

        if g.line > 0 {
            let pot_parts_top = self.part_nums_in_line(g.line - 1);
            for p in pot_parts_top {
                if part_is_adjacent_col(&p, g) {
                    nums.push(p.nr);
                }
            }
        }

        let pot_parts_line = self.part_nums_in_line(g.line);
        for p in pot_parts_line {
            if part_is_adjacent_col(&p, g) {
                nums.push(p.nr);
            }
        }

        if g.line < self.field.len() - 1 {
            let pot_parts_bot = self.part_nums_in_line(g.line + 1);
            for p in pot_parts_bot {
                if part_is_adjacent_col(&p, g) {
                    nums.push(p.nr);
                }
            }
        }
        if nums.len() == 2 {
            Some(nums[0] * nums[1])
        } else {
            None
        }
    }
}
impl From<&str> for Schematic {
    fn from(s: &str) -> Self {
        let mut schem = Self { field: Vec::new() };
        for l in s.lines() {
            schem.field.push(l.chars().collect())
        }
        schem
    }
}
impl fmt::Debug for Schematic {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        writeln!(f, "")?;
        for l in self.field.iter() {
            write!(f, "  ")?;
            for c in l {
                write!(f, "{c}")?;
            }
            writeln!(f, "")?;
        }
        Ok(())
    }
}

fn part_is_adjacent_col(p: &PartNr, g: &Gear) -> bool {
    let p_end = p.col + p.nr.to_string().len() - 1;
    p.col == g.col - 1
        || p.col == g.col
        || p.col == g.col + 1
        || p_end == g.col - 1
        || p_end == g.col
        || p_end == g.col + 1
}

fn analyze_part_nrs_pt1(l: &str) -> u32 {
    let s = Schematic::from(l);
    s.pot_part_numbers()
        .iter()
        .filter(|pt_nr| s.is_valid_part_nr(pt_nr))
        .map(|pt_nr| pt_nr.nr)
        .sum()
}

fn analyze_gear_ratios_pt2(l: &str) -> u32 {
    let s = Schematic::from(l);
    let gears = s.find_gears();
    gears.iter().filter_map(|g| s.gear_ratio(g)).sum()
}

fn main() {
    let f = fs::read_to_string("input.txt").unwrap();

    let pt1: u32 = analyze_part_nrs_pt1(&f);
    println!("part1: {pt1}");

    let pt2: u32 = analyze_gear_ratios_pt2(&f);
    println!("part2: {pt2}");
}

#[cfg(test)]
mod tests {
    use super::*;
    //use test::Bencher;

    const INP1: &str = "467..114..
...*......
..35..633.
......#...
617*......
.....+.58.
..592.....
......755.
...$.*....
.664.598..";

    const SOLS1: [(PartNr, bool); 10] = [
        (
            PartNr {
                nr: 467,
                line: 0,
                col: 0,
            },
            true,
        ),
        (
            PartNr {
                nr: 114,
                line: 0,
                col: 5,
            },
            false,
        ),
        (
            PartNr {
                nr: 35,
                line: 2,
                col: 2,
            },
            true,
        ),
        (
            PartNr {
                nr: 633,
                line: 2,
                col: 6,
            },
            true,
        ),
        (
            PartNr {
                nr: 617,
                line: 4,
                col: 0,
            },
            true,
        ),
        (
            PartNr {
                nr: 58,
                line: 5,
                col: 7,
            },
            false,
        ),
        (
            PartNr {
                nr: 592,
                line: 6,
                col: 2,
            },
            true,
        ),
        (
            PartNr {
                nr: 755,
                line: 7,
                col: 6,
            },
            true,
        ),
        (
            PartNr {
                nr: 664,
                line: 9,
                col: 1,
            },
            true,
        ),
        (
            PartNr {
                nr: 598,
                line: 9,
                col: 5,
            },
            true,
        ),
    ];

    #[test]
    fn part_numbers_test() {
        let s = Schematic::from(INP1);
        dbg!(&s);
        println!("{:?}", s.pot_part_numbers());
        for (pt_nr, (sol, valid)) in s.pot_part_numbers().iter().zip(SOLS1.iter()) {
            println!("--------");
            assert_eq!(pt_nr, sol);
            println!("{:?}", (sol, valid));
            assert_eq!(s.is_valid_part_nr(pt_nr), *valid);
        }
    }

    #[test]
    fn pt1_test() {
        assert_eq!(analyze_part_nrs_pt1(INP1), 4361)
    }

    #[test]
    fn gear_finding_test() {
        let s = Schematic::from(INP1);
        dbg!(&s);
        let gears = s.find_gears();
        println!("{gears:?}");
        let sols = [
            Gear { line: 1, col: 3 },
            Gear { line: 4, col: 3 },
            Gear { line: 8, col: 5 },
        ];
        assert_eq!(gears, sols);
    }

    #[test]
    fn test_validate_gears() {
        let s = Schematic::from(INP1);
        dbg!(&s);
        let pt_nrs = s.pot_part_numbers();
        assert_eq!(s.gear_ratio(&Gear { line: 1, col: 3 }), Some(16345));
        assert_eq!(s.gear_ratio(&Gear { line: 8, col: 5 }), Some(451490));
        assert_eq!(s.gear_ratio(&Gear { line: 4, col: 3 }), None);
    }

    #[test]
    fn pt2_test() {
        assert_eq!(analyze_gear_ratios_pt2(INP1), 467835)
    }
}
