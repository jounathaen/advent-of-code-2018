use std::{collections::HashSet, fmt::Debug, fs};

#[derive(Copy, Clone, PartialEq)]
enum SpaceType {
    Void,
    Galaxy,
}
impl From<u8> for SpaceType {
    fn from(b: u8) -> Self {
        match b {
            b'.' => Self::Void,
            b'#' => Self::Galaxy,
            _ => panic!("Invalid input char {}", b as char),
        }
    }
}
impl Debug for SpaceType {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let c = match self {
            SpaceType::Void => ' ',
            SpaceType::Galaxy => '#',
        };
        write!(f, "{c}")
    }
}

#[derive(Clone, PartialEq)]
struct Space {
    f: Vec<SpaceType>,
    width: usize,
    height: usize,
    galaxies: HashSet<(usize, usize)>,
}
impl Space {
    fn get(&self, pos: (usize, usize)) -> SpaceType {
        self.f[pos.1 * self.width + pos.0]
    }

    fn get_expanded_galaxies(&self, factor: usize) -> Vec<(usize, usize)> {
        let mut expansion_cols = HashSet::<usize>::from_iter(0..self.width);
        let mut expansion_lines = HashSet::<usize>::from_iter(0..self.height);

        for (x, y) in self.galaxies.iter() {
            expansion_cols.remove(x);
            expansion_lines.remove(y);
        }
        let (new_width, new_height) = (
            (self.width + expansion_cols.len()),
            (self.height + expansion_lines.len()),
        );

        let mut expansion_cols = expansion_cols.into_iter().collect::<Vec<usize>>();
        expansion_cols.sort();
        let mut expansion_lines = expansion_lines.into_iter().collect::<Vec<usize>>();
        expansion_lines.sort();

        fn build_map(expansion_slots: &[usize], max_len: usize, factor: usize) -> Vec<usize> {
            let mut map = Vec::new();
            let mut past_slot = 0;
            let mut insert_slots = 0;
            for ec in expansion_slots.iter() {
                map.extend((past_slot..*ec).map(|c| c + insert_slots));
                past_slot = *ec;
                insert_slots += factor - 1;
            }
            map.extend((past_slot..max_len).map(|c| c + insert_slots));
            map
        }
        let col_map = build_map(&expansion_cols, new_width, factor);
        let line_map = build_map(&expansion_lines, new_height, factor);

        self.galaxies
            .iter()
            .map(|(x, y)| (col_map[*x], line_map[*y]))
            .collect()
    }
}
impl From<&str> for Space {
    fn from(s: &str) -> Self {
        let width = s.lines().next().unwrap().as_bytes().len();
        let height = s.lines().count();
        let mut f = Vec::with_capacity(width * height);
        let mut galaxies = HashSet::new();
        for (i, l) in s.lines().enumerate() {
            l.as_bytes()
                .iter()
                .copied()
                .map(SpaceType::from)
                .enumerate()
                .for_each(|(j, t)| {
                    if t == SpaceType::Galaxy {
                        galaxies.insert((j, i));
                    }
                    f.push(t)
                })
        }
        Self {
            f,
            width,
            height,
            galaxies,
        }
    }
}
impl Debug for Space {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("Field")
            .field("width", &self.width)
            .field("height", &self.height)
            .field("galaxies", &self.galaxies)
            .finish()?;
        writeln!(f, "\nspace:")?;
        writeln!(f, "   x")?;
        write!(f, "   ")?;
        for x in 0..self.width {
            if x <= 9 {
                write!(f, "{x}")?;
            } else {
                write!(f, "v")?;
            }
        }
        writeln!(f)?;
        for y in 0..self.height {
            if y <= 9 {
                if y == 0 {
                    write!(f, "y{y} ")?;
                } else {
                    write!(f, " {y} ")?;
                }
            } else {
                write!(f, " > ")?;
            }
            for x in 0..self.width {
                write!(f, "{:?}", self.get((x, y)))?;
            }
            writeln!(f)?;
        }
        write!(f, "")
    }
}

fn manhattan_distance(g1: (usize, usize), g2: (usize, usize)) -> usize {
    let x_dist = if g1.0 > g2.0 {
        g1.0 - g2.0
    } else {
        g2.0 - g1.0
    };
    let y_dist = if g1.1 > g2.1 {
        g1.1 - g2.1
    } else {
        g2.1 - g1.1
    };
    x_dist + y_dist
}

fn galaxy_distances(gal_vec: &[(usize, usize)]) -> usize {
    let mut dist_sum = 0;
    for i in 0..gal_vec.len() {
        for j in i..gal_vec.len() {
            dist_sum += manhattan_distance(gal_vec[i], gal_vec[j]);
        }
    }
    dist_sum
}

fn main() {
    let f = fs::read_to_string("input.txt").unwrap();

    let space = Space::from(f.as_str());
    let pt1 = galaxy_distances(&space.get_expanded_galaxies(2));
    println!("part1: {}", pt1);

    let pt2 = galaxy_distances(&space.get_expanded_galaxies(1_000_000));
    println!("part2: {}", pt2);
}

#[cfg(test)]
mod tests {
    use super::*;

    const INP1: &str = "...#......
.......#..
#.........
..........
......#...
.#........
.........#
..........
.......#..
#...#.....";

    const INP_EXP: &str = "....#........
.........#...
#............
.............
.............
........#....
.#...........
............#
.............
.............
.........#...
#....#.......";

    #[test]
    fn parse_test() {
        let space = Space::from(INP1);
        dbg!(&space);
        assert_eq!(space.get((0, 0)), SpaceType::Void);
        assert_eq!(space.get((3, 0)), SpaceType::Galaxy);
        assert_eq!(space.get((0, 2)), SpaceType::Galaxy);
        assert_eq!(space.get((9, 1)), SpaceType::Void);

        assert_eq!(space.galaxies.len(), 9);
    }

    #[test]
    fn expansion_test() {
        let space = Space::from(INP1);
        let expanded = Space::from(INP_EXP);
        let mut g1 = space.get_expanded_galaxies(2);
        g1.sort();
        let mut g2 = expanded
            .galaxies
            .iter()
            .copied()
            .collect::<Vec<(usize, usize)>>();
        g2.sort();
        assert_eq!(g1, g2);
    }

    #[test]
    fn manhattan_distance_test() {
        assert_eq!(manhattan_distance((4, 0), (9, 10)), 15);
        assert_eq!(manhattan_distance((0, 2), (12, 7)), 17);
        assert_eq!(manhattan_distance((0, 11), (5, 11)), 5);
    }

    #[test]
    fn gal_distance_test() {
        let space = Space::from(INP1);
        assert_eq!(galaxy_distances(&space.get_expanded_galaxies(2)), 374);
        assert_eq!(galaxy_distances(&space.get_expanded_galaxies(10)), 1030);
        assert_eq!(galaxy_distances(&space.get_expanded_galaxies(100)), 8410);
    }
}
